/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "change_slithy.h"
#include "evedit.h"

const int   ChangeSlithy::MAX_COMBO_0 = 5;

ChangeSlithy::ChangeSlithy(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"amount",amount);
		GetAttribute(attribs,"change",change);
		if(change == "add")
			change = "Add to size";
		else
			change = "Set size to";
	}
}

ChangeSlithy::ChangeSlithy(const std::string *ev_params,const std::string *scr_params)
{
	amount = scr_params[EDIT0];
	change = scr_params[COMBO0];
}

ChangeSlithy::~ChangeSlithy()
{

}


void	ChangeSlithy::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "changeslithy";
	display[Script::LEVEL] = staff_only;

	display[LABEL0] = "Amount";
	display[EDIT0]  = amount;

	std::string combo_0[MAX_COMBO_0];
	combo_0[0] = "How to change";
	combo_0[1] = "0";
	combo_0[2] = "Add to size";
	combo_0[3] = "Set size to";
	combo_0[4] = "";

	if(change.find("Set") == 0)
		combo_0[1] = "1";

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	ChangeSlithy::Save(std::ofstream& file)
{
	file << "         <changeslithy amount='" << amount << "'";
	if(change.find("Add" == 0))
		file << " change='add'" << "/>\n"; 
	else
		file << " change='set'" << "/>\n"; 
}

void	ChangeSlithy::Update(const std::string *ev_params,const std::string *scr_params)
{
	amount = scr_params[EDIT0];
	change = scr_params[COMBO0];

}

bool	ChangeSlithy::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me an amount for the change!");
		return(false);
	}

	return(true);
}

void	ChangeSlithy::Write(std::ostringstream& buffer)
{
	buffer << "changeslithy script:  amount " << amount;
	buffer << ": change using " << change;
}

