/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "delay_event.h"
#include "evedit.h"

DelayEvent::DelayEvent(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"delay",delay);
		GetAttribute(attribs,"event",event);
		GetAttribute(attribs,"logoff",logoff);
	}
}

DelayEvent::DelayEvent(const std::string *ev_params,const std::string *scr_params)
{
	delay  = scr_params[EDIT0];
	event  = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	logoff = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

DelayEvent::~DelayEvent()
{

}


void	DelayEvent::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "delayevent";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Delay";
	display[Script::EDIT0] = delay;
	display[Script::LABEL1] = "Event";
	display[Script::EDIT1] = event;
	display[Script::LABEL2] = "Log Off";
	display[Script::EDIT2] = logoff;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	DelayEvent::Save(std::ofstream& file)
{
	file << "         <delayevent";
	file << " delay='" << delay << "'";
	if(event != "")	
		file << " event='" << event << "'";
	if(logoff != "")
		file << " logoff='" << logoff << "'";
	file << "/>\n";
}

void	DelayEvent::Update(const std::string *ev_params,const std::string *scr_params)
{
	delay = scr_params[EDIT0];
	event = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	logoff = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

bool	DelayEvent::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT1] == "") && (scr_params[EDIT2] == ""))
	{
		EvEdit::MainWindow()->Message("You need to give me an event to call!");
		return(false);
	}
	if(scr_params[EDIT0] == "")
		EvEdit::MainWindow()->Message("You haven't told me how long you want to delay the event!");
	return(true);
}

void	DelayEvent::Write(std::ostringstream& buffer)
{
	buffer << "delayevent script: delay " << delay;
	if(event != "")
		buffer << ":  event " << event;
	if(logoff != "")
		buffer << "  log off " << logoff;
}





