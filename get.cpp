/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "get.h"

#include "evedit.h"


const int   Get::MAX_COMBO_0 = 5;

Get::Get(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"id-name",id_name);
		GetAttribute(attribs,"silent",silent);
		(silent == "true") ? silent = "y" : silent = "n";
	}
}

Get::Get(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	silent = scr_params[COMBO0];
}

Get::~Get()
{

}


void	Get::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "get";
	display[Script::LEVEL] = planet_owners;

	display[LABEL0] = "ID or name";
	display[EDIT0] = id_name;

	std::string combo_0[MAX_COMBO_0];
	combo_0[0] = "Silent?";
	combo_0[1] = "1";
	combo_0[2] = "Yes";
	combo_0[3] = "No";
	combo_0[4] = "";

	if(silent == "Yes")
	{
		std::ostringstream	buffer;
		buffer << 0;
		combo_0[1] = buffer.str();
	}

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	Get::Save(std::ofstream& file)
{
	file << "         <get id-name='" << id_name << "'";
	if(silent == "Yes")		file << " silent='true'";
	file << "/>\n";
}

void	Get::Update(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	silent = scr_params[COMBO0];
}

bool	Get::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me an object to get!");
		return(false);
	}
	else
		return(true);
}

void	Get::Write(std::ostringstream& buffer)
{
	buffer << "get script:  id-name " << id_name;
	if(silent == "Yes")		buffer << ":  silent yes";
}

