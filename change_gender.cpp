/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "change_gender.h"
#include "evedit.h"


const int	ChangeGender::MAX_COMBO_0 = 6;

ChangeGender::ChangeGender(const QXmlAttributes *attribs)
{
	if(attribs != 0)
		GetAttribute(attribs,"gender",gender);
	if(gender == "f")
		gender = "female";
	else
	{
		if(gender == "m")
			gender = "male";
		else
		{
			if(gender == "n")
				gender = "neuter";
		}
	}
}

ChangeGender::ChangeGender(const std::string *ev_params,const std::string *scr_params)
{
	gender = scr_params[COMBO0];
}

ChangeGender::~ChangeGender()
{

}


void	ChangeGender::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "changegender";
	display[Script::LEVEL] = staff_only;

	// Change MAX_COMBO_0 if you add to this!
	std::string	combo_0[MAX_COMBO_0];
	combo_0[0] = "Gender";
	combo_0[1] = "0";
	combo_0[2] = "female";
	combo_0[3] = "male";
	combo_0[4] = "neuter";
	combo_0[5] = "";

	if(gender == "male")			combo_0[1] = "1";
	if(gender == "neuter")		combo_0[1] = "2";

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	ChangeGender::Save(std::ofstream& file)
{
	file << "         <changegender gender='";
	if(gender == "male")			file << "m" << "'/>\n";
	if(gender == "female")		file << "f" << "'/>\n";
	if(gender == "neuter")		file << "n" << "'/>\n";
}

void	ChangeGender::Update(const std::string *ev_params,const std::string *scr_params)
{
	gender = scr_params[COMBO0];
}

void	ChangeGender::Write(std::ostringstream& buffer)
{
	buffer << "changegender script:  gender " << gender;
}



