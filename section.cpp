/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "section.h"

#include <fstream>
#include <sstream>

#include <cctype>
#include <cstdlib>

#include "script.h"
#include "evedit.h"
#include "event.h"

Section::~Section()
{
	for(EventIndex::iterator iter = index.begin();iter != index.end();++iter)
		delete iter->second;
}


void	Section::AddEvent(Event *event)
{
	index[event->Number()] = event;
}

void	Section::AddScript2Current(const std::string *ev_params,const std::string *scr_params)
{
	Event	*event = Find(std::atoi(ev_params[Script::NUMBER].c_str()));
	if((event != 0) && (event == current))
	{
		current->AddScript(ev_params,scr_params);
		return;
	}
}

void	Section::ClearAllCurrent()
{
	if(current !=0)
		current->ClearCurrent();
	current = 0;
}

bool	Section::DeleteCurrentEvent()
{
	if(current == 0)
	{
		EvEdit::MainWindow()->Error("You haven't selected an event to delete!");
		return(false);
	}
	
	std::ostringstream	buffer;
	buffer << "Delete event:\n\n'#" << current->NumberAndComment();
	buffer << "'\nand all its scripts.\n\nAre you sure?";
	if(!EvEdit::MainWindow()->Warning(buffer.str()))
		return(false);
	
	EventIndex::iterator iter = index.find(current->Number());
	if(iter == index.end())
	{
		EvEdit::MainWindow()->Message("I can't seem to find that event\nPlease report this problem to feedback");
		return(false);
	}
	else
	{
		Event	*event = iter->second;
		index.erase(iter);
		delete event;
		current = 0;
		EvEdit::MainWindow()->ClearFileEventDisplay();
		EvEdit::MainWindow()->ClearEventDisplay();
		EvEdit::MainWindow()->ClearListing();
		EvEdit::MainWindow()->ClearScriptDisplay();
		EvEdit::MainWindow()->ClearScriptListDisplay();
		return(true);
	}
}

void	Section::DeleteScript(int which)
{
	if(current != 0)
		current->DeleteScript(which);
}

void	Section::DisplayEventList()
{
	std::list<std::string>	event_list;
	for(EventIndex::iterator iter = index.begin();iter != index.end();++iter)
		event_list.push_back(iter->second->NumberAndComment());
	EvEdit::MainWindow()->DisplayEventList(event_list);
}

void	Section::DisplayScript(int which)
{
	if(current == 0)
		EvEdit::MainWindow()->Message("Unable to find the event for this script\nPlease report this problem to ibgames");
	else
		current->DisplayScript(which);
}

void	Section::DisplayScriptList(const std::string& cat_name,const std::string& event_number)
{
	current = Find(std::atoi(event_number.c_str()));
	if(current == 0)
		EvEdit::MainWindow()->Message("Unable to find the selected event\nPlease report this problem to ibgames");
	else
		current->DisplayScriptList(cat_name,name);
}

bool	Section::EventExists(const std::string *ev_params)
{
	Event *event = Find(std::atoi(ev_params[Script::NUMBER].c_str()));
	if(event == 0)
		return(false);
	else
		return(true);
}

Event	*Section::Find(int event_number)
{
	EventIndex::iterator iter = index.find(event_number);
	if(iter != index.end())
		return(iter->second);
	else
		return(0);
}

bool	Section::IsCurrentEvent(const std::string *ev_params)
{
	if(current == 0)
		return(false);

	Event	*event = Find(std::atoi(ev_params[Script::NUMBER].c_str()));
	if(event != current)
		return(false);
	else
		return(true);
}

void	Section::List()
{
	std::ostringstream	buffer;
	buffer << "  Start section: " << name;
	EvEdit::MainWindow()->List(buffer,"green");
	for(EventIndex::iterator iter = index.begin();iter != index.end();++iter)
		iter->second->List();	
	buffer.str("");
	buffer << "  End section: " << name;;
	EvEdit::MainWindow()->List(buffer,"green");
}

void	Section::MoveScript(int which,int how)
{
	if(current != 0)
		current->MoveScript(which,how);
}

void	Section::NewEvent(const std::string *ev_params)
{
	int ev_number = std::atoi(ev_params[Script::NUMBER].c_str());
	Event *event = Find(ev_number);

	if(event == 0)
	{
		EvEdit::MainWindow()->ClearFileEventDisplay();
		event = new Event(ev_number);
		AddEvent(event);
		event->AddComment(ev_params[Script::COMMENT]);
		current = event;
		DisplayEventList();
	}
}

int	Section::NextFreeEvNumber()
{
	if(index.size() == 0)
		return(1);

	EventIndex::iterator iter = --index.end();
	return(iter->first + 1);
}

int	Section::NumScriptsInCurEvent()
{
	if(current != 0)
		return(current->NumScripts());
	else
		return(0);
}

void	Section::Save(std::ofstream& file)
{
	if(index.size() > 0)
	{
		file << "      <section name='" << name << "'>\n";
		for(EventIndex::iterator iter = index.begin();iter != index.end();++iter)
			iter->second->Save(file);
		file << "\n      </section>\n";
	}
}

void	Section::UpdateComment(const std::string& text)
{
	if(current != 0)
		current->AddComment(text);
}

void	Section::UpdateScript(const std::string *ev_params,const std::string *scr_params,int which)
{
	if(current != 0)
		current->UpdateScript(ev_params,scr_params,which);
}

