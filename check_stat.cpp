/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_stat.h"
#include "evedit.h"

#include <cctype>

CheckStat::CheckStat(const QXmlAttributes *attribs)
{
	SetUpCombo0();
	if(attribs != 0)
	{
		GetAttribute(attribs,"who",who);
		std::string	temp;
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			GetAttribute(attribs,combo_0[count],temp);
			if(temp != "")
			{
				stat = combo_0[count];
				value = temp;
				break;
			}
		}
		GetAttribute(attribs,"higher",higher);
		GetAttribute(attribs,"equals",equals);
		GetAttribute(attribs,"lower",lower);
	}
}

CheckStat::CheckStat(const std::string *ev_params,const std::string *scr_params)
{
	SetUpCombo0();
	stat = scr_params[COMBO0];
	who = scr_params[COMBO1];
	value = scr_params[EDIT0];
	higher = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
	lower = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
}

CheckStat::~CheckStat()
{

}


void CheckStat::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];

	display[SCRIPT] = "checkstat";
	display[LEVEL] = staff_only;

	display[LABEL0] = "Amount";
	display[EDIT0] = value;
	display[Script::LABEL1] = "Higher than";
	display[Script::EDIT1] = higher;
	display[Script::LABEL2] = "Equals";
	display[Script::EDIT2] = equals;
	display[Script::LABEL3] = "Lower than";
	display[Script::EDIT3] = lower;

	if(stat != "")
	{
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			if(stat == combo_0[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_0[1] = buffer.str();
				break;
			}
		}
	}
	else
		combo_0[1] = "0";

	std::string combo_1[MAX_COMBO_1];
	combo_1[0] = "Who to check";
	combo_1[1] = "0";
	combo_1[2] = "individual";
	combo_1[3] = "room";
	combo_1[4] = "room-ex";
	combo_1[5] = "";

	if(who != "")
	{
		for(int count = 2; count < MAX_COMBO_1 - 1;++count)
		{
			if(who == combo_1[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_1[1] = buffer.str();
				break;
			}
		}
	}
	EvEdit::MainWindow()->DisplayScript(display,combo_0,combo_1);
}

void	CheckStat::Save(std::ofstream& file)
{
	file << "         <checkstat who='" << who << "'";
	file << " " << stat << "='" << value << "'";
	if(higher != "")
		file << " higher='" << higher << "'";
	if(equals != "")
		file << " equals='" << equals << "'";
	if(lower != "")
		file << " lower='" << lower << "'";
	file << "/>\n";
}

void	CheckStat::SetUpCombo0()
{
	combo_0[0]  = "Stat name";
	combo_0[1]  = "0";
	combo_0[2]  = "strength";
	combo_0[3]  = "stamina";
	combo_0[4]  = "dexterity";
	combo_0[5]  = "intelligence";
	combo_0[6]  = "money";
	combo_0[7]  = "reward";
	combo_0[8]  = "ak";
	combo_0[9]  = "tc";
	combo_0[10] = "total";
	combo_0[11] = "rank";
	combo_0[12] = "c-money";
	combo_0[13] = "customs";
	combo_0[14] = "killed";
	combo_0[15] = "remote-check";
	combo_0[16] = "shares";
	combo_0[17] = "treasury";
	combo_0[18] = "hc";
	combo_0[19] = "";
}

void	CheckStat::Update(const std::string *ev_params, const std::string *scr_params)
{
	stat = scr_params[COMBO0];
	who = scr_params[COMBO1];
	value = scr_params[EDIT0];
	higher = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
	lower = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
}

bool	CheckStat::Validate(const std::string *scr_params)
{	
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't told me what\namount to check for!\n");
		return(false);
	}
	if((scr_params[EDIT1] == "") && (scr_params[EDIT2] == "") && (scr_params[EDIT3] == ""))
	{
		EvEdit::MainWindow()->Message("You need to give me at least\none event to call!\n");
		return(false);
	}
	return(true);
}

void	CheckStat::Write(std::ostringstream& buffer)
{
	buffer << "checkstat script:  who " << who;
	buffer << ":  " << stat << " " << value;
	if(higher != "")
		buffer << ":  " << "higher " << higher;
	if(equals != "")
		buffer << ":  " << "equals " << equals;
	if(lower != "")
		buffer << ":  " << "lower " << lower;
}

