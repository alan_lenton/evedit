/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef CATEGORY_H
#define CATEGORY_H

#include <map>
#include <string>

#include <fstream>

class Section;
class Event;

typedef	std::map<const std::string,Section *,std::less< std::string> >	SectionIndex;

class Category
{
private:
	std::string		name;
	Section			*current;
	SectionIndex	index;

	Category(const Category& rhs);
	Category& operator=(const Category& rhs);
	
	Section	*Find(const std::string& section_name);

public:
	Category(const std::string cat_name) :name(cat_name), current(0)	{	}
	~Category();
	
	const std::string&	Name()		{ return(name);	}

	int	NextFreeEvNumber();
	int	NextFreeEvNumber(const std::string *ev_params);
	int	NumScriptsInCurEvent();

	bool	DeleteCurrentEvent();
	bool	DeleteCurrentSection();
	bool	EventExists(const std::string *ev_params);
	bool	IsCurrentEvent(const std::string *ev_params);

	void	AddEvent(Event *event);
	void	AddScript2Current(const std::string *ev_params,const std::string *scr_params);
	void	AddSection(Section *section);
	void	ClearAllCurrent();
	void	DeleteScript(int which);
	void	DisplayEventList(const std::string& section_name);
	void	DisplayScript(int which);
	void	DisplayScriptList(const std::string& event_number);
	void	DisplaySectionList();
	void	List();
	void	MoveScript(int which,int how);
	void	NewEvent(const std::string *ev_params);
	void	Save(std::ofstream& file);
	void	UpdateComment(const std::string& text);						
	void	UpdateScript(const std::string *ev_params,const std::string *scr_params,int which);						
};

#endif
