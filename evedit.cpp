/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "evedit.h"

#include <list>
#include <sstream>
#include <utility>

#include <QCloseEvent>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFontDialog>
#include <QLabel>
#include <QMessageBox>
#include <QPainter>
#include <QSettings>
#include <QTreeWidget>
#include <QUrl>

#include	"evfile.h"
#include "script.h"
#include "script_factory.h"

EvEdit	*EvEdit::main_window = 0;

EvEdit::EvEdit(QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	home = QDir::currentPath().toStdString();

	version = "Version 2.03";
	ev_file =  new EvFile();
	new_script = changed = false;

	std::ostringstream	buffer;
	buffer << "EvEdit - Federation 2 Events Editor - " << version;
	setWindowTitle(buffer.str().c_str());

	CreateActions();
	CreateMainMenu();
	CreateStatusBar();
	SetUpConnections();
	SetUpScriptNames();

	QSettings settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "EvEdit");
	QFont	font(settings.value("font-type", "verdana").toString(), settings.value("font-size", 9).toInt());
	ui.centralWidget->setFont(font);
	ui.script_names->setFont(font);
	ui.script_list->setFont(font);
	ui.events_list->setFont(font);
	ui.section_list->setFont(font);
	ui.category_list->setFont(font);
	status_label->setFont(font);
	statusBar()->setFont(font);

	resize(settings.value("win-size", QSize(780, 540)).toSize());
	move(settings.value("win-pos", QPoint(20, 20)).toPoint());
	directory = settings.value("cur-dir", "./").toString();

	picture = QImage(":/EvEdit/Resources/karakatoa1.png");
	ui.image_widget->installEventFilter(this);
	
	main_window = this;
}

EvEdit::~EvEdit()
{
	delete ev_file;

	QSettings settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "EvEdit");
	settings.setValue("font-type", ui.centralWidget->font().family());
	settings.setValue("font-size", ui.centralWidget->font().pointSize());
	settings.setValue("win-size", size());
	settings.setValue("win-pos", pos());
	settings.setValue("cur-dir", directory);
	
}


void	EvEdit::About()
{
	QString	buffer = "Federation 2 Scripting Language Editor\n";
	buffer += "Copyright (c) 1985-2013 Alan Lenton\n";
	buffer += "Released under the GNU General Public License v3\n";
	buffer += "http://www.gnu.org/copyleft/gpl.html\n\n";
	buffer += "Design and programming by Alan Lenton\n";
	buffer += "Testing and manuals by Fiona Craig\n";
	buffer += "Website: www.ibgames.com\n";
	buffer += "Email: feedback@ibgames.com\n\n";
	buffer += "git: https://bitbucket.org/alanxxx/msgeditor/overview";
	QMessageBox::information(this,"About Federation 2 Script Editor",buffer);
}

void	EvEdit::AddScript(const std::string& display_line)
{
	ui.script_list->addItem(QString::fromStdString(display_line));
	changed = true;
}

void	EvEdit::ClearEventDisplay()
{
	ui.category_edit->setText("");
	ui.section_edit->setText("");
	ui.number_spin->setValue(1);
	ui.comment_edit->setText("");
}

void	EvEdit::ClearScriptDisplay()
{
	ui.script_label->setText("");		ui.level_edit->setText("");	ui.level_edit->setEnabled(false);
	ui.edit_label_0->setText("");		ui.edit_0->setText("");			ui.edit_0->setEnabled(false);			ui.edit_0->setToolTip("");
	ui.edit_label_1->setText("");		ui.edit_1->setText("");			ui.edit_1->setEnabled(false);			ui.edit_1->setToolTip("");
	ui.edit_label_2->setText("");		ui.edit_2->setText("");			ui.edit_2->setEnabled(false);			ui.edit_2->setToolTip("");
	ui.edit_label_3->setText("");		ui.edit_3->setText("");			ui.edit_3->setEnabled(false);			ui.edit_3->setToolTip("");
	ui.edit_label_4->setText("");		ui.edit_4->setText("");			ui.edit_4->setEnabled(false);			ui.edit_4->setToolTip("");
	ui.edit_label_5->setText("");		ui.edit_5->setText("");			ui.edit_5->setEnabled(false);			ui.edit_5->setToolTip("");
	ui.combo_label_0->setText("");	ui.combo_0->clear();				ui.combo_0->setEnabled(false);
	ui.combo_label_1->setText("");	ui.combo_1->clear();				ui.combo_1->setEnabled(false);
	ui.text_label->setText("");		ui.text_edit->setText("");		ui.text_edit->setEnabled(false);
}

void	EvEdit::Close()
{
	close();
}

void	EvEdit::closeEvent(QCloseEvent *event)
{
	if(ev_file->CanClose())
		event->accept();
	else
		event->ignore();
}

void	EvEdit::CreateActions()
{ 
	about_action = new QAction("About",this);
	about_action->setStatusTip("Program info");

	copy_event_action = new QAction("Copy Event",this);
	copy_event_action->setStatusTip("Make a copy of an event");

	del_cat_action = new QAction("Delete Current Category",this);
	del_cat_action->setStatusTip("Delete selected category");

	del_event_action = new QAction("Delete Current Event",this);
	del_event_action->setStatusTip("Delete selected event");

	del_script_action = new QAction("Delete Current Script",this);
	del_script_action->setStatusTip("Delete selected script");
	
	del_section_action = new QAction("Delete Current Section",this);
	del_section_action->setStatusTip("Delete selected scection");
	
	exit_action = new QAction("E&xit",this);
	exit_action->setShortcut(tr("Ctrl+x"));
	exit_action->setStatusTip("Close the events editor");

	font_action = new QAction("Choose &Font",this);
	font_action->setShortcut(tr("Ctrl+f"));
	font_action->setStatusTip("Change the font");
	
	load_action = new QAction("&Load",this);
	load_action->setShortcut(tr("Ctrl+l"));
	load_action->setStatusTip("Load file from disk");
	
	new_action = new QAction("&New",this);
	new_action->setShortcut(tr("Ctrl+n"));
	new_action->setStatusTip("Start a new set of events");
	
	paste_event_action = new QAction("Paste Event",this);
	paste_event_action->setStatusTip("Paste a copied event into the editor");
	paste_event_action->setEnabled(false);

	save_action = new QAction("&Save",this);
	save_action->setShortcut(tr("Ctrl+s"));
	save_action->setStatusTip("Save to disk file");
	
	saveas_action = new QAction("Save as...",this);
	saveas_action->setStatusTip("Save to a different file");
	
	help_action = new QAction("&Help",this);
	help_action->setShortcut(tr("Ctrl+h"));
	help_action->setStatusTip("Access online help");

	test_action = new QAction("Test",this);
	test_action->setStatusTip("Run tests");
}

void	EvEdit::CreateMainMenu()
{
	file_menu = menuBar()->addMenu("File");
	file_menu->addAction(new_action);
	file_menu->addSeparator();
	file_menu->addAction(load_action);
	file_menu->addAction(save_action);
	file_menu->addAction(saveas_action);
	file_menu->addSeparator();
	file_menu->addAction(exit_action);	

	edit_menu = menuBar()->addMenu("Edit");
	edit_menu->addAction(copy_event_action);
	edit_menu->addAction(paste_event_action);
	edit_menu->addSeparator();
	edit_menu->addAction(del_cat_action);
	edit_menu->addAction(del_section_action);
	edit_menu->addAction(del_event_action);
	edit_menu->addAction(del_script_action);
	
	tools_menu = menuBar()->addMenu("Tools"); 
	tools_menu->addAction(font_action);

	help_menu = menuBar()->addMenu("Help"); 
	help_menu->addAction(help_action);
	help_menu->addSeparator();
	help_menu->addAction(about_action);
}

void	EvEdit::CreateStatusBar()
{
	status_label = new QLabel(this);
	statusBar()->addPermanentWidget(status_label);
	statusBar()->showMessage(QString::fromLatin1("Ready..."));
}

void	EvEdit::DeleteCategory()
{
	ev_file->DeleteCurrentCategory();
}

void	EvEdit::DeleteEvent()
{
	ev_file->DeleteCurrentEvent();
}

void	EvEdit::DeleteScript()
{
	int which = ui.script_list->currentRow();
	if(which >= 0)
		ev_file->DeleteScript(which);	
}

void	EvEdit::DeleteSection()
{
	ev_file->DeleteCurrentSection();
}

void	EvEdit::DisplayCategoryList(const std::list<std::string>& cat_names)
{
	ui.category_list->clear();
	ui.section_list->clear();
	ui.events_list->clear();
	ui.script_list->clear();
	std::list<std::string>::const_iterator	iter;
	for(iter = cat_names.begin();iter != cat_names.end();++iter)
		ui.category_list->addItem((*iter).c_str());
}

void	EvEdit::DisplayEvent(const std::string *ev_list)
{
	ClearEventDisplay();

	ui.category_edit->setText(QString::fromStdString(ev_list[Script::CATEGORY]));
	ui.section_edit->setText(QString::fromStdString(ev_list[Script::SECTION]));
	ui.number_spin->setValue(std::atoi(ev_list[Script::NUMBER].c_str()));
	ui.comment_edit->setText(QString::fromStdString(ev_list[Script::COMMENT]));
}

void	EvEdit::DisplayEventList(const std::list<std::string>& ev_list)
{
	ui.events_list->clear();
	ui.script_list->clear();
	std::list<std::string>::const_iterator	iter;
	for(iter = ev_list.begin();iter != ev_list.end();++iter)
		ui.events_list->addItem((*iter).c_str());
}

void	EvEdit::DisplayScript(const std::string *scr_list,
									const std::string *combo_0,const std::string *combo_1)
{
	ClearScriptDisplay();

	ui.script_label->setText(QString::fromStdString(scr_list[Script::SCRIPT]));
	ui.level_edit->setText(QString::fromStdString(scr_list[Script::LEVEL]));
	ui.level_edit->setEnabled(true);

	// must do combo boxes first, because they can trigger a signal 
	// which clears edit boxes 
	if(combo_0 != 0)
	{
		ui.combo_label_0->setText(QString::fromStdString(combo_0[0]));
		for(int index = 2;combo_0[index] != "";++index)
			ui.combo_0->addItem(QString::fromStdString(combo_0[index]));
		ui.combo_0->setCurrentIndex(std::atoi(combo_0[1].c_str()));
		ui.combo_0->setEnabled(true);
	}
	
	if(combo_1 != 0)
	{
		ui.combo_label_1->setText(QString::fromStdString(combo_1[0]));
		for(int index = 2;combo_1[index] != "";++index)
			ui.combo_1->addItem(QString::fromStdString(combo_1[index]));
		ui.combo_1->setCurrentIndex(std::atoi(combo_1[1].c_str()));
		ui.combo_1->setEnabled(true);
	}
	
	if(scr_list[Script::LABEL0] != "")
	{
		ui.edit_label_0->setText(QString::fromStdString(scr_list[Script::LABEL0]));
		ui.edit_0->setEnabled(true);
		ui.edit_0->setText(QString::fromStdString(scr_list[Script::EDIT0]));
		ui.edit_0->setToolTip(ui.edit_0->text());
	}
	if(scr_list[Script::LABEL1] != "")
	{
		ui.edit_label_1->setText(QString::fromStdString(scr_list[Script::LABEL1]));
		ui.edit_1->setEnabled(true);
		ui.edit_1->setText(QString::fromStdString(scr_list[Script::EDIT1]));
		ui.edit_1->setToolTip(ui.edit_1->text());
	}
	if(scr_list[Script::LABEL2] != "")
	{
		ui.edit_label_2->setText(QString::fromStdString(scr_list[Script::LABEL2]));
		ui.edit_2->setEnabled(true);
		ui.edit_2->setText(QString::fromStdString(scr_list[Script::EDIT2]));
		ui.edit_2->setToolTip(ui.edit_2->text());
	}
	if(scr_list[Script::LABEL3] != "")
	{
		ui.edit_label_3->setText(QString::fromStdString(scr_list[Script::LABEL3]));
		ui.edit_3->setEnabled(true);
		ui.edit_3->setText(QString::fromStdString(scr_list[Script::EDIT3]));
		ui.edit_3->setToolTip(ui.edit_3->text());
	}
	if(scr_list[Script::LABEL4] != "")
	{
		ui.edit_label_4->setText(QString::fromStdString(scr_list[Script::LABEL4]));
		ui.edit_4->setEnabled(true);
		ui.edit_4->setText(QString::fromStdString(scr_list[Script::EDIT4]));
		ui.edit_4->setToolTip(ui.edit_4->text());
	}
	if(scr_list[Script::LABEL5] != "")
	{
		ui.edit_label_5->setText(QString::fromStdString(scr_list[Script::LABEL5]));
		ui.edit_5->setEnabled(true);
		ui.edit_5->setText(QString::fromStdString(scr_list[Script::EDIT5]));
		ui.edit_5->setToolTip(ui.edit_5->text());
	}

	if(scr_list[Script::TEXTLAB] != "")
	{
		ui.text_label->setText(QString::fromStdString(scr_list[Script::TEXTLAB]));
		ui.text_edit->setEnabled(true);
		ui.text_edit->setText(QString::fromStdString(scr_list[Script::TEXTEDIT]));
	}
}

void	EvEdit::DisplayScriptList(const std::list<std::string>& scr_list)
{
	ui.script_list->clear();
	std::list<std::string>::const_iterator	iter;
	for(iter = scr_list.begin();iter != scr_list.end();++iter)
		ui.script_list->addItem((*iter).c_str());
}

void	EvEdit::DisplaySectionList(const std::list<std::string>& section_names)
{
	ui.section_list->clear();
	ui.events_list->clear();
	ui.script_list->clear();
	std::list<std::string>::const_iterator	iter;
	for(iter = section_names.begin();iter != section_names.end();++iter)
		ui.section_list->addItem((*iter).c_str());
}

void	EvEdit::Error(const std::string& text)
{
	QMessageBox::critical(this,tr("EvEdit"),QString::fromStdString(text));
}

bool EvEdit::eventFilter(QObject *target,QEvent *event)
{
	if((target == ui.image_widget) && (event->type() == QEvent::Paint))
	{
		QPainter	painter(ui.image_widget);
		painter.drawImage(0,0,picture);
		return(true);
	}
	return(QWidget::eventFilter(target,event));
}

void	EvEdit::Help()
{
	QString	buffer("http://www.ibgames.net/fed2/workbench/advanced/scripts/index.html");
	QDesktopServices::openUrl(buffer);
}

void	EvEdit::List(std::ostringstream& buffer,const std::string colour)
{
	ui.listing_edit->setTextColor(QColor(QString::fromStdString(colour)));
	ui.listing_edit->append(QString::fromStdString(buffer.str()));
}

void	EvEdit::Load()
{
	if(ev_file->Changed())
	{
		switch(QMessageBox::question(this,tr("Event Editor"),tr("Save existing file?"),
					QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, QMessageBox::Cancel))
		{
			case QMessageBox::Yes :		Save();	break;
			case QMessageBox::Cancel:	return;
		}
	}
	
	QString	file_name = QFileDialog::getOpenFileName(this,tr("Load File"),
								directory,tr("Event Files (*.xev);;All files(*.*)"));
	if(!file_name.isNull())
	{
		directory = file_name;
		int index = directory.lastIndexOf('/');
		if(index == -1)
			directory = "./";
		else
			directory.truncate(index);

		ev_file->FileName(file_name.toStdString());
		ev_file->Load();
	}
}

void	EvEdit::Message(const std::string& text)
{
	QMessageBox::information(this,tr("EvEdit"),QString::fromStdString(text));
}

void	EvEdit::NewSet()
{
	if(ev_file->CanClose())
	{
		ev_file->Clear();
		ev_file->ClearFileName();
		ClearEventDisplay();
		ClearFileEventDisplay();
		ClearFileSectionDisplay();
		ClearListing();
		ClearScriptDisplay();
		ClearScriptListDisplay();
		ClearFileCategoryDisplay();
	}
}

void	EvEdit::OnAddScriptBtnClicked(bool checked)
{
	std::string	ev_params[Script::MAX_EVENT_DISPLAY];
	ev_params[Script::CATEGORY] = ui.category_edit->text().toStdString();
	ev_params[Script::SECTION] = ui.section_edit->text().toStdString();
	std::ostringstream	buffer;
	buffer << ui.number_spin->value();
	ev_params[Script::NUMBER] = buffer.str();
	ev_params[Script::COMMENT] = ui.comment_edit->text().toStdString();

	std::string	scr_params[Script::MAX_SCRIPT_DISPLAY];
	scr_params[Script::SCRIPT] = ui.script_label->text().toStdString();
	scr_params[Script::EDIT0] = ui.edit_0->text().toStdString();
	scr_params[Script::EDIT1] = ui.edit_1->text().toStdString();
	scr_params[Script::EDIT2] = ui.edit_2->text().toStdString();
	scr_params[Script::EDIT3] = ui.edit_3->text().toStdString();
	scr_params[Script::EDIT4] = ui.edit_4->text().toStdString();
	scr_params[Script::EDIT5] = ui.edit_5->text().toStdString();
	scr_params[Script::COMBO0] = ui.combo_0->currentText().toStdString();
	scr_params[Script::COMBO1] = ui.combo_1->currentText().toStdString();
	scr_params[Script::TEXTEDIT] = ui.text_edit->document()->toPlainText().toStdString();

	ev_file->AddScript(ev_params,scr_params);
}

void EvEdit::OnCategoryListClicked(QListWidgetItem *item)
{
	ev_file->CategorySelected(item->text().toStdString());
}

void	EvEdit::OnClearScriptBtnClicked(bool checked)
{
	ClearScriptDisplay();
}

void	EvEdit::OnCombo0Changed(const QString& text)
{
	std::string	scr_params[Script::MAX_SCRIPT_DISPLAY];
	ev_file->GetComboUpdateInfo(Script::COMBO0,text.toStdString(),ui.script_label->text().toStdString(),scr_params);
	UpdateScriptDisplay(scr_params);
}

void	EvEdit::OnCombo1Changed(const QString& text)
{
	std::string	scr_params[Script::MAX_SCRIPT_DISPLAY];
	ev_file->GetComboUpdateInfo(Script::COMBO1,text.toStdString(),ui.script_label->text().toStdString(),scr_params);
	UpdateScriptDisplay(scr_params);
}

void	EvEdit::OnCommentEditFinished()
{
	ev_file->UpdateComment(ui.comment_edit->text().toStdString());
}

void	EvEdit::OnEventsListClicked(QListWidgetItem *item)
{
	ev_file->EventSelected(item->text().toStdString());
}

void	EvEdit::OnLineEditChanged(const QString& text)
{
	QLineEdit *edit = dynamic_cast<QLineEdit *>(sender());
	if(edit != 0)
		edit->setToolTip(text);
}

void	EvEdit::OnNewEventBtnClicked(bool checked)
{
	std::string	ev_params[Script::MAX_EVENT_DISPLAY];
	ev_params[Script::CATEGORY] = ui.category_edit->text().toStdString();
	ev_params[Script::SECTION] = ui.section_edit->text().toStdString();
	std::ostringstream	buffer;
	buffer << ui.number_spin->value();
	ev_params[Script::NUMBER] = buffer.str();
	ev_params[Script::COMMENT] = ui.comment_edit->text().toStdString();

	ev_file->NewEvent(ev_params);
}

void	EvEdit::OnNextEventBtnClicked(bool checked)
{
	std::string	ev_params[Script::MAX_EVENT_DISPLAY];
	ev_params[Script::CATEGORY] = ui.category_edit->text().toStdString();
	ev_params[Script::SECTION] = ui.section_edit->text().toStdString();
	std::ostringstream	buffer;
	buffer << ui.number_spin->value();
	ev_params[Script::NUMBER] = buffer.str();
	ev_params[Script::COMMENT] = ui.comment_edit->text().toStdString();

	ev_file->NextEvent(ev_params);
}

void	EvEdit::OnNotebookPageChanged(int index)
{
	if(index == 2)
		ev_file->List();
}

void	EvEdit::OnScriptDownBtnClicked(bool checked)
{
	int which = ui.script_list->currentRow();
	if(which >= 0)
		ev_file->MoveScript(which,MOVE_DOWN);
}

void  EvEdit::OnScriptListClicked(QListWidgetItem *item)
{
	if(ev_file->ScriptSelected(ui.script_list->currentRow()))
		new_script = false;
}

void  EvEdit::OnScriptNamesClicked(QListWidgetItem *item)
{
	if(!ev_file->HasCurrentCategory())
		Error("You haven't set up a category\nand section for the event!");
	else
	{
		if(ev_file->NewScript(item->text().toStdString()))
			new_script = true;
	}
}

void	EvEdit::OnScriptNamesDoubleClicked(QListWidgetItem *item)
{
	QString	buffer("http://www.ibgames.net/fed2/workbench/advanced/scripts/");
	buffer += item->text();
	buffer += ".html";
	QDesktopServices::openUrl(buffer);
}

void	EvEdit::OnScriptUpBtnClicked(bool checked)
{
	int which = ui.script_list->currentRow();
	if(which >= 0)
		ev_file->MoveScript(which,MOVE_UP);
}

void	EvEdit::OnScriptUpdateBtnClicked(bool checked)
{
	int which = ui.script_list->currentRow();
	if(which == -1)
		return;

	std::string	ev_params[Script::MAX_EVENT_DISPLAY];
	ev_params[Script::CATEGORY] = ui.category_edit->text().toStdString();
	ev_params[Script::SECTION] = ui.section_edit->text().toStdString();
	std::ostringstream	buffer;
	buffer << ui.number_spin->value();
	ev_params[Script::NUMBER] = buffer.str();
	ev_params[Script::COMMENT] = ui.comment_edit->text().toStdString();

	std::string	scr_params[Script::MAX_SCRIPT_DISPLAY];
	scr_params[Script::SCRIPT] = ui.script_label->text().toStdString();
	scr_params[Script::EDIT0] = ui.edit_0->text().toStdString();
	scr_params[Script::EDIT1] = ui.edit_1->text().toStdString();
	scr_params[Script::EDIT2] = ui.edit_2->text().toStdString();
	scr_params[Script::EDIT3] = ui.edit_3->text().toStdString();
	scr_params[Script::EDIT4] = ui.edit_4->text().toStdString();
	scr_params[Script::EDIT5] = ui.edit_5->text().toStdString();
	scr_params[Script::COMBO0] = ui.combo_0->currentText().toStdString();
	scr_params[Script::COMBO1] = ui.combo_1->currentText().toStdString();
	scr_params[Script::TEXTEDIT] = ui.text_edit->document()->toPlainText().toStdString();

	ev_file->UpdateScript(ev_params,scr_params,which);
	ClearScriptDisplay();
}

void EvEdit::OnSectionListClicked(QListWidgetItem *item)
{
	ev_file->SectionSelected(item->text().toStdString());
}

void	EvEdit::OnTextChanged()
{
	changed = true;
}

void	EvEdit::Save()
{
	ev_file->Save();
}

void	EvEdit::SaveAs()
{
	QString	file_name = QFileDialog::getSaveFileName(this,tr("Save File"),directory,tr("Event Files (*.xev);;All files(*.*)"));
	if(!file_name.isNull())
	{
		directory = file_name;
		int index = directory.lastIndexOf('/');
		if(index == -1)
			directory = "./";
		else
			directory.truncate(index);

		ev_file->FileName(file_name.toStdString());
		ev_file->Save();
		WriteStatusBar(file_name.toStdString());
	}
}

void	EvEdit::SelectCategoryList(const std::string& label)
{
	QString	text(label.c_str());
	int total = ui.category_list->count();
	for(int count = 0;count < total;++count)
	{
		if(ui.category_list->item(count)->text() == text)
		{
			ui.category_list->setCurrentRow(count);
			return;
		}
	}

	ui.category_list->setCurrentRow(-1);
	return;
}

void	EvEdit::SelectFont()
{
	bool ok;
	QFont	original_font(ui.centralWidget->font());
	QFont font = QFontDialog::getFont(&ok,ui.centralWidget->font(),this);
	if(!ok) 
		font = original_font;

	ui.centralWidget->setFont(font);
	ui.script_names->setFont(font);
	ui.script_list->setFont(font);
	ui.events_list->setFont(font);
	ui.section_list->setFont(font);
	ui.category_list->setFont(font);
	status_label->setFont(font);
}

void	EvEdit::SelectSectionList(const std::string& label)
{
	QString	text(label.c_str());
	int total = ui.section_list->count();
	for(int count = 0;count < total;++count)
	{
		if(ui.section_list->item(count)->text() == text)
		{
			ui.section_list->setCurrentRow(count);
			return;
		}
	}

	ui.section_list->setCurrentRow(-1);
	return;
}

void	EvEdit::SetButtonEnableStatus(int which,bool new_status)
{
	switch(which)
	{
		case ADD_SCRIPT_BTN:			ui.add_script_btn->setEnabled(new_status);		break;
		case UPDATE_SCRIPT_BTN:		ui.update_script_btn->setEnabled(new_status);	break;
		case CLEAR_SCRIPT_BTN:		ui.clear_script_btn->setEnabled(new_status);		break;
		case NEW_EVENT_BTN:			ui.new_event_btn->setEnabled(new_status);			break;
		case NEXT_EVENT_BTN:			ui.next_event_btn->setEnabled(new_status);		break;
		case MOVE_SCRIPT_UP_BTN:	ui.script_up_btn->setEnabled(new_status);			break;
		case MOVE_SCRIPT_DOWN_BTN:	ui.script_down_btn->setEnabled(new_status);		break;
	}
}

void	EvEdit::SetUpConnections()
{
	// action connects
	connect(about_action,SIGNAL(triggered()),this,SLOT(About()));
	connect(copy_event_action,SIGNAL(triggered()),this,SLOT(Copy()));
	connect(del_cat_action,SIGNAL(triggered()),this,SLOT(DeleteCategory()));
	connect(del_event_action,SIGNAL(triggered()),this,SLOT(DeleteEvent()));
	connect(del_script_action,SIGNAL(triggered()),this,SLOT(DeleteScript()));
	connect(del_section_action,SIGNAL(triggered()),this,SLOT(DeleteSection()));
	connect(exit_action,SIGNAL(triggered()),this,SLOT(Close()));
	connect(help_action,SIGNAL(triggered()),this,SLOT(Help()));
	connect(load_action,SIGNAL(triggered()),this,SLOT(Load()));
	connect(new_action,SIGNAL(triggered()),this,SLOT(NewSet()));
	connect(paste_event_action,SIGNAL(triggered()),this,SLOT(Paste()));
	connect(save_action,SIGNAL(triggered()),this,SLOT(Save()));
	connect(saveas_action,SIGNAL(triggered()),this,SLOT(SaveAs()));
	connect(font_action,SIGNAL(triggered()),this,SLOT(SelectFont()));
	connect(test_action,SIGNAL(triggered()),this,SLOT(Test()));

	// other connections
	connect(ui.add_script_btn,SIGNAL(clicked(bool)),this,SLOT(OnAddScriptBtnClicked(bool)));
	connect(ui.category_list,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(OnCategoryListClicked(QListWidgetItem *)));
	connect(ui.clear_script_btn,SIGNAL(clicked(bool)),this,SLOT(OnClearScriptBtnClicked(bool)));
	connect(ui.combo_0,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(OnCombo0Changed(const QString&)));
	connect(ui.combo_1,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(OnCombo1Changed(const QString&)));
	connect(ui.comment_edit,SIGNAL(editingFinished()),this,SLOT(OnCommentEditFinished()));
	connect(ui.events_list,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(OnEventsListClicked(QListWidgetItem *)));
	connect(ui.new_event_btn,SIGNAL(clicked(bool)),this,SLOT(OnNewEventBtnClicked(bool)));
	connect(ui.next_event_btn,SIGNAL(clicked(bool)),this,SLOT(OnNextEventBtnClicked(bool)));
	connect(ui.notebook,SIGNAL(currentChanged(int)),this,SLOT(OnNotebookPageChanged(int)));
	connect(ui.script_down_btn,SIGNAL(clicked(bool)),this,SLOT(OnScriptDownBtnClicked(bool)));
	connect(ui.script_list,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(OnScriptListClicked(QListWidgetItem *)));
	connect(ui.script_names,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(OnScriptNamesClicked(QListWidgetItem *)));
	connect(ui.script_names,SIGNAL(itemDoubleClicked(QListWidgetItem *)),this,SLOT(OnScriptNamesDoubleClicked(QListWidgetItem *)));
	connect(ui.script_up_btn,SIGNAL(clicked(bool)),this,SLOT(OnScriptUpBtnClicked(bool)));
	connect(ui.update_script_btn,SIGNAL(clicked(bool)),this,SLOT(OnScriptUpdateBtnClicked(bool)));
	connect(ui.section_list,SIGNAL(itemClicked(QListWidgetItem *)),this,SLOT(OnSectionListClicked(QListWidgetItem *)));
	connect(ui.text_edit,SIGNAL(textChanged()),this,SLOT(OnTextChanged()));

	// all the line edit boxes connect to the same slot
	connect(ui.edit_0,SIGNAL(textChanged(const QString &)),this,SLOT(OnLineEditChanged(const QString&)));
	connect(ui.edit_1,SIGNAL(textChanged(const QString &)),this,SLOT(OnLineEditChanged(const QString&)));
	connect(ui.edit_2,SIGNAL(textChanged(const QString &)),this,SLOT(OnLineEditChanged(const QString&)));
	connect(ui.edit_3,SIGNAL(textChanged(const QString &)),this,SLOT(OnLineEditChanged(const QString&)));
	connect(ui.edit_4,SIGNAL(textChanged(const QString &)),this,SLOT(OnLineEditChanged(const QString&)));
	connect(ui.edit_5,SIGNAL(textChanged(const QString &)),this,SLOT(OnLineEditChanged(const QString&)));
}

void	EvEdit::SetUpScriptNames()
{
	ScriptFactory	*factory = ScriptFactory::GetFactory();
	for(int count = 0;factory->names[count] != "";++count)
		new QListWidgetItem(factory->names[count].c_str(),ui.script_names);
}

void	EvEdit::Test()
{
	std::ostringstream	buffer;
	buffer << "The .ini file is at\n   " << Home() << "/EvEdit.ini";
	Message(buffer.str());
}

void	EvEdit::UpdateScriptDisplay(const std::string *scr_list)
{
	if(scr_list[Script::LABEL0] != "")
	{
		if(scr_list[Script::LABEL0] != "~disable")
		{
			ui.edit_label_0->setText(QString::fromStdString(scr_list[Script::LABEL0]));
			ui.edit_0->setEnabled(true);
			ui.edit_0->setText(QString::fromStdString(scr_list[Script::EDIT0]));
			ui.edit_0->setToolTip(ui.edit_0->text());
		}
		else
		{
			ui.edit_label_0->setText("");
			ui.edit_0->setText("");
			ui.edit_0->setEnabled(false);
			ui.edit_0->setToolTip("");
		}
	}
	if(scr_list[Script::LABEL1] != "")
	{
		if(scr_list[Script::LABEL1] != "~disable")
		{
			ui.edit_label_1->setText(QString::fromStdString(scr_list[Script::LABEL1]));
			ui.edit_1->setEnabled(true);
			ui.edit_1->setText(QString::fromStdString(scr_list[Script::EDIT1]));
			ui.edit_1->setToolTip(ui.edit_1->text());
		}
		else
		{
			ui.edit_label_1->setText("");
			ui.edit_1->setText("");
			ui.edit_1->setEnabled(false);
			ui.edit_1->setToolTip("");
		}
	}
	if(scr_list[Script::LABEL2] != "")
	{
		if(scr_list[Script::LABEL2] != "~disable")
		{
			ui.edit_label_2->setText(QString::fromStdString(scr_list[Script::LABEL2]));
			ui.edit_2->setEnabled(true);
			ui.edit_2->setText(QString::fromStdString(scr_list[Script::EDIT2]));
			ui.edit_2->setToolTip(ui.edit_2->text());
		}
		else
		{
			ui.edit_label_2->setText("");
			ui.edit_2->setText("");
			ui.edit_2->setEnabled(false);
			ui.edit_2->setToolTip("");
		}
	}
	if(scr_list[Script::LABEL3] != "")
	{
		if(scr_list[Script::LABEL3] != "~disable")
		{
			ui.edit_label_3->setText(QString::fromStdString(scr_list[Script::LABEL3]));
			ui.edit_3->setEnabled(true);
			ui.edit_3->setText(QString::fromStdString(scr_list[Script::EDIT3]));
			ui.edit_3->setToolTip(ui.edit_3->text());
		}
		else
		{
			ui.edit_label_3->setText("");
			ui.edit_3->setText("");
			ui.edit_3->setEnabled(false);
			ui.edit_3->setToolTip("");
		}
	}
	if(scr_list[Script::LABEL4] != "")
	{
		if(scr_list[Script::LABEL4] != "~disable")
		{
			ui.edit_label_4->setText(QString::fromStdString(scr_list[Script::LABEL4]));
			ui.edit_4->setEnabled(true);
			ui.edit_4->setText(QString::fromStdString(scr_list[Script::EDIT4]));
			ui.edit_4->setToolTip(ui.edit_4->text());
		}
		else
		{
			ui.edit_label_4->setText("");
			ui.edit_4->setText("");
			ui.edit_4->setEnabled(false);
			ui.edit_4->setToolTip("");
		}
	}
	if(scr_list[Script::LABEL5] != "")
	{
		if(scr_list[Script::LABEL5] != "~disable")
		{
			ui.edit_label_5->setText(QString::fromStdString(scr_list[Script::LABEL5]));
			ui.edit_5->setEnabled(true);
			ui.edit_5->setText(QString::fromStdString(scr_list[Script::EDIT5]));
			ui.edit_5->setToolTip(ui.edit_5->text());
		}
		else
		{
			ui.edit_label_5->setText("");
			ui.edit_5->setText("");
			ui.edit_5->setEnabled(false);
			ui.edit_5->setToolTip("");
		}
	}

	if(scr_list[Script::TEXTLAB] != "")
	{
		if(scr_list[Script::TEXTLAB] != "~disable")
		{
			ui.text_label->setText(QString::fromStdString(scr_list[Script::TEXTLAB]));
			ui.text_edit->setEnabled(true);
			ui.text_edit->setText(QString::fromStdString(scr_list[Script::TEXTEDIT]));
		}
		else
		{
			ui.text_label->setText("");
			ui.text_edit->setText("");
			ui.text_edit->setEnabled(false);
		}
	}
}

bool	EvEdit::Warning(const std::string& text)
{
	int ret = QMessageBox::warning(this, tr("EvEdit"),QString::fromStdString(text),
					               QMessageBox::Yes | QMessageBox::Cancel,QMessageBox::Cancel);
   if(ret == QMessageBox::Yes)
		return(true);
	else
		return(false);
}

void	EvEdit::WriteStatusBar(const std::string& text)
{
	status_label->setText(QString::fromStdString(text));
}

void	EvEdit::WriteTemp2StatusBar(const std::string& text)
{
	statusBar()->showMessage(QString::fromStdString(text),5000);
}



/*************************** work in progress *************************/

void	EvEdit::Copy()
{
	paste_event_action->setEnabled(true);
	Message("Not available yet");
}

void	EvEdit::Paste()
{
	Message("Not available yet");
}

