/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "script.h"

#include <cctype>

//#include "announce.h"


const std::string Script::mssg_types[] = { "single", "multi", "text", ""	};
const std::string	Script::planet_owners = "Planet Owners";
const std::string	Script::staff_only = "***Staff Only***";


Script::~Script()
{

}


std::string&	Script::CleanText(std::string& source)
{
	int	len = source.length();
	if(len == 0)
		return(source);
		
	int	last = source.find_last_not_of(' ');	// find any trailing spaces
	if(last == std::string::npos)
		last = len - 1;
	int	first = source.find_first_not_of(' ');	// leading spaces
	if(first == std::string::npos)
		first = 0;
		
	std::ostringstream	buffer;
	for(int count = first;count <= last;++count)
	{
		if(source[count] == '\n')						// convert to space
			buffer << ' ';
		else
		{
			if(!(source[count] == '\r'))				// drop completely
			buffer << source[count];
		}
	}
	return(source = buffer.str());
}


std::string&	Script::EscapeXML(std::string& source)
{
	std::ostringstream	buffer;
	int	len = source.length();
	for(int count = 0;count <len;++count)
	{
		switch(source[count])
		{
			case  '<':	buffer << "&lt;";		break;
			case  '>':	buffer << "&gt;";		break;
			case  '&':	buffer << "&amp;";	break;
			case '\'':	buffer << "&apos;";	break;
			case '\"':	buffer << "&quot;";	break;
			default	:	if(isascii(source[count]) == 0)
								buffer << '#';
							else
								buffer << source[count];
							break;
		}
	}
	source = buffer.str();
	return(source);
}

int	Script::FindTextType(const std::string& low,
							const std::string& high,const std::string& text)
{
	if((low.length() != 0) && (high.length() != 0))
		return(MULTI);
	else
	{
		if(text.length() != 0)
			return(TEXT);
		else
			return(SINGLE);
	}
}

void	Script::GetAttribute(const QXmlAttributes *attribs,
									const std::string& name,std::string& value)
{
	value = (attribs->value(QString::fromStdString(name))).toStdString();
}

const std::string&	Script::MakeFullName(const std::string& cat,const std::string& sect,const std::string& num_text)
{
	static std::string	ret_val;
	std::ostringstream	buffer;

	if((isdigit(*(num_text.c_str()))) || (num_text.find("random[") == 0))
	{
		buffer << cat << "." << sect << "." << num_text;
		ret_val = buffer.str();
		return(ret_val);
	}
	else
		return(num_text);
}
