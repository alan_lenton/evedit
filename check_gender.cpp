/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_gender.h"
#include "evedit.h"

CheckGender::CheckGender(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"male",male);
		GetAttribute(attribs,"female",female);
		GetAttribute(attribs,"neuter",neuter);
	}
}

CheckGender::CheckGender(const std::string *ev_params,const std::string *scr_params)
{
	male = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	female = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	neuter = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

CheckGender::~CheckGender()
{

}


void	CheckGender::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checkgender";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Male";
	display[Script::EDIT0] = male;
	display[Script::LABEL1] = "Female";
	display[Script::EDIT1] = female;
	display[Script::LABEL2] = "Neuter";
	display[Script::EDIT2] = neuter;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckGender::Save(std::ofstream& file)
{
	file << "         <checkgender";
	if(male != "")
		file << " male='" << male << "'";
	if(female != "")
		file << " female='" << female << "'";
	if(neuter != "")
		file << " neuter='" << neuter << "'";
	file << "/>\n";
}

void	CheckGender::Update(const std::string *ev_params,const std::string *scr_params)
{
	male = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	female = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	neuter = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

bool	CheckGender::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == "") && (scr_params[EDIT2] == ""))
	{
		EvEdit::MainWindow()->Message("You need to give me an event to\ncall for at least one of the genders!");
		return(false);
	}
	return(true);
}

void	CheckGender::Write(std::ostringstream& buffer)
{
	bool first = true;
	buffer << "checkgender script:";
	if(male != "")
	{
		buffer << "  male event " << male;
		first = false;
	}
	if(female != "")
	{
		if(!first)
			buffer << ":";
		buffer << "  female event " << female;
		first = false;
	}
	if(neuter != "")
	{
		if(!first)
			buffer << ":";
		buffer << "  neuter event " << neuter;
	}
}


