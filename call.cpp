/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "call.h"

#include <cctype>

#include "evedit.h"


Call::Call(const QXmlAttributes *attribs)
{
	if(attribs != 0)
		GetAttribute(attribs,"event",ev_num);
}

Call::Call(const std::string *ev_params,const std::string *scr_params)
{
	ev_num = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
}

Call::~Call()
{

}


void	Call::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];
	display[SCRIPT] = "call";
	display[LEVEL] = planet_owners;
	
	display[LABEL0] = "Event";
	display[EDIT0] = ev_num;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	Call::Save(std::ofstream& file)
{
	file << "         <call event='" << ev_num << "'/>\n";
}

void	Call::Update(const std::string *ev_params,const std::string *scr_params)
{
	ev_num = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
}

bool	Call::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't said what event you want to call!\n");
		return(false);
	}
	else
		return(true);
}

void	Call::Write(std::ostringstream& buffer)
{
	buffer << "call script:  event " << ev_num;
}

