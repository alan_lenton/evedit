/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_map.h"

#include "evedit.h"

CheckMap::CheckMap(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"map",map_name);
		GetAttribute(attribs,"loc",loc_no);
		GetAttribute(attribs,"pass",pass);
		GetAttribute(attribs,"fail",fail);
	}
}

CheckMap::CheckMap(const std::string *ev_params,const std::string *scr_params)
{
	map_name = scr_params[EDIT0];
	loc_no = scr_params[EDIT1];
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

CheckMap::~CheckMap()
{

}


void	CheckMap::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];
	display[SCRIPT] = "checkmap";
	display[LEVEL] = planet_owners;
	
	display[LABEL0] = "Map name";
	if(map_name != "")
		display[EDIT0] = map_name;
	else
		display[EDIT0] = "home";
	display[LABEL1] = "Location #";
	display[EDIT1] = loc_no;
	display[LABEL3] = "Pass";
	display[EDIT3] = pass;
	display[LABEL4] = "Fail";
	display[EDIT4] = fail;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckMap::Save(std::ofstream& file)
{
	file << "         <checkmap map='" << map_name << "'";
	if(loc_no != "")
		file << " loc='" << loc_no << "'";
	if(pass != "")
		file << " pass='" << pass << "'";
	if(fail != "")
		file << " fail='" << fail << "'";
	file <<"/>\n";
}

void	CheckMap::Update(const std::string *ev_params,const std::string *scr_params)
{
	map_name = scr_params[EDIT0];
	loc_no = scr_params[EDIT1];
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

bool	CheckMap::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a\nmap to check against!");
		return(false);
	}
	if((scr_params[EDIT3] == "") && (scr_params[EDIT4] == ""))
	{
		EvEdit::MainWindow()->Message("I need at least a pass or a\nfail event for this script!");
		return(false);
	}
	return(true);
}

void	CheckMap::Write(std::ostringstream& buffer)
{
	buffer << "checkmap script:  map-name " << map_name;
	if(loc_no != "")
		buffer << ":  location " << loc_no;
	if(pass != "")
		buffer << ":  pass " << pass;
	if(fail != "")
		buffer << ":  fail " << fail;
}

