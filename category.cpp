/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "category.h"

#include "evedit.h"
#include "script.h"
#include "section.h"

Category::~Category()
{
	for(SectionIndex::iterator iter = index.begin();iter != index.end();++iter)
		delete iter->second;
}


void	Category::AddEvent(Event *event)
{
	if(current != 0)
		current->AddEvent(event);
}

void	Category::AddScript2Current(const std::string *ev_params,const std::string *scr_params)
{
	Section	*section = Find(ev_params[Script::SECTION]);
	if((current != 0) && (section == current))
	{
		current->AddScript2Current(ev_params,scr_params);
		return;
	}
}

void	Category::AddSection(Section *section)
{
	index[section->Name()] = section;
	current = section;
}

void	Category::ClearAllCurrent()
{
	if(current !=0)
		current->ClearAllCurrent();
	current = 0;
}

bool	Category::DeleteCurrentEvent()
{
	if(current == 0)
	{
		EvEdit::MainWindow()->Error("You haven't selected an event to delete!");
		return(false);
	}
	else
	{
		if(current->DeleteCurrentEvent())
		{
			DisplaySectionList();
			return(true);
		}
		else
			return(false);
	}
}

bool	Category::DeleteCurrentSection()
{
	if(current == 0)
	{
		EvEdit::MainWindow()->Error("You haven't selected a section to delete!");
		return(false);
	}

	std::ostringstream	buffer;
	buffer << "Delete section:\n\n'" << current->Name();
	buffer << "' and all its events.\n\nAre you sure?";
	if(!EvEdit::MainWindow()->Warning(buffer.str()))
		return(false);
	
	SectionIndex::iterator iter = index.find(current->Name());
	if(iter == index.end())
	{
		EvEdit::MainWindow()->Message("I can't seem to find that section\nPlease report this problem to feedback");
		return(false);
	}
	else
	{
		Section	*section = iter->second;
		index.erase(iter);
		delete section;
		current = 0;
		EvEdit::MainWindow()->ClearFileEventDisplay();
		EvEdit::MainWindow()->ClearFileSectionDisplay();
		EvEdit::MainWindow()->ClearEventDisplay();
		EvEdit::MainWindow()->ClearListing();
		EvEdit::MainWindow()->ClearScriptDisplay();
		EvEdit::MainWindow()->ClearScriptListDisplay();
		return(true);
	}
}

void	Category::DeleteScript(int which)
{
	if(current != 0)
		current->DeleteScript(which);
}

void	Category::DisplayEventList(const std::string& section_name)
{
	current = Find(section_name);
	if(current != 0)
		current->DisplayEventList();
}

void	Category::DisplayScript(int which)
{
	if(current == 0)
		EvEdit::MainWindow()->Message("Unable to find the section for this script\nPlease report this problem to ibgames");
	else
		current->DisplayScript(which);
}

void	Category::DisplayScriptList(const std::string& event_number)
{
	if(current == 0)
		EvEdit::MainWindow()->Message("Unable to find the section for this event\nPlease report this problem to ibgames");
	else
		current->DisplayScriptList(name,event_number);
}

void	Category::DisplaySectionList()
{
	std::list<std::string>	section_list;
	for(SectionIndex::iterator iter = index.begin();iter != index.end();++iter)
		section_list.push_back(iter->second->Name());
	EvEdit::MainWindow()->DisplaySectionList(section_list);
}

bool	Category::EventExists(const std::string *ev_params)
{
	Section *section = Find(ev_params[Script::SECTION]);
	if(section == 0)
		return(false);
	else
		return(section->EventExists(ev_params));
}

Section	*Category::Find(const std::string& section_name)
{
	SectionIndex::iterator	iter = index.find(section_name);
	if(iter != index.end())
		return(iter->second);
	else
		return(0);
}

bool	Category::IsCurrentEvent(const std::string *ev_params)
{
	if(current == 0)
		return(false);

	Section	*section = Find(ev_params[Script::SECTION]);
	if(section != current)
		return(false);
	else
		return(current->IsCurrentEvent(ev_params));
}

void	Category::List()
{
	std::ostringstream	buffer;
	buffer << "Start category: " << name;
	EvEdit::MainWindow()->List(buffer,"blue");
	for(SectionIndex::iterator iter = index.begin();iter != index.end();++iter)
		iter->second->List();
	buffer.str("");
	buffer << "End category: " << name << "\n";
	EvEdit::MainWindow()->List(buffer,"blue");
}

void	Category::MoveScript(int which,int how)
{
	if(current != 0)
		current->MoveScript(which,how);
}

void	Category::NewEvent(const std::string *ev_params)
{
	Section	*section = Find(ev_params[Script::SECTION]);

	if(section == 0)	// need to set up a new section
	{
		EvEdit::MainWindow()->ClearFileSectionDisplay();
		EvEdit::MainWindow()->ClearFileEventDisplay();
		section = new Section(ev_params[Script::SECTION]);
		AddSection(section);
		current = section;
		DisplaySectionList();		
		EvEdit::MainWindow()->SelectSectionList(ev_params[Script::SECTION]);
	}
	section->NewEvent(ev_params);
}

int	Category::NextFreeEvNumber()
{
	if(current != 0)
		return(current->NextFreeEvNumber());
	else
		return(1);
}

int	Category::NextFreeEvNumber(const std::string *ev_params)
{
	Section	*section = Find(ev_params[Script::SECTION]);
	if(section != 0)
		return(section->NextFreeEvNumber());
	else
		return(1);
}

int	Category::NumScriptsInCurEvent()
{
	if(current != 0)
		return(current->NumScriptsInCurEvent());
	else
		return(0);
}

void	Category::Save(std::ofstream& file)
{
	if(index.size() > 0)
	{
		file << "   <category name='" << name << "'>\n";
		for(SectionIndex::iterator iter = index.begin();iter != index.end();++iter)
			iter->second->Save(file);
		file << "   </category>\n";
	}
}

void	Category::UpdateComment(const std::string& text)
{
	if(current != 0)
		current->UpdateComment(text);
}

void	Category::UpdateScript(const std::string *ev_params,const std::string *scr_params,int which)
{
	if(current != 0)
		current->UpdateScript(ev_params,scr_params,which);
}

