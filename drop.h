/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef DROP_H
#define DROP_H

#include	<string>

#include "script.h"

class Drop : public Script
{
	static const int MAX_COMBO_0;

private:
	std::string	id_name;
	std::string	silent;

	Drop(const Drop& rhs);
	Drop& operator=(const Drop& rhs);

public:
	static bool	Validate(const std::string *scr_params);

	Drop(const QXmlAttributes *attribs = 0);
	Drop(const std::string *ev_params,const std::string *scr_params);
	~Drop();

	void	Display();
	void	Save(std::ofstream& file);
	void	Update(const std::string *ev_params,const std::string *scr_params);
	void	Write(std::ostringstream& buffer);
};

#endif
