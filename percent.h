/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef PERCENT_H
#define PERCENT_H

#include	<string>

#include "script.h"

class Percent : public Script
{
private:
	std::string	value;
	std::string	higher;
	std::string	equals;
	std::string	lower;
	
	Percent(const Percent& rhs);
	Percent& operator=(const Percent& rhs);

public:
	static bool	Validate(const std::string *scr_params);

	Percent(const QXmlAttributes *attribs = 0);
	Percent(const std::string *ev_params,const std::string *scr_params);
	~Percent();

	void	Display();
	void	Save(std::ofstream& file);
	void	Update(const std::string *ev_params,const std::string *scr_params);
	void	Write(std::ostringstream& buffer);
};

#endif
