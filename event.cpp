/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "event.h"

#include <algorithm>
#include <sstream>

#include "evedit.h"
#include "script.h"
#include "script_factory.h"


Event::~Event()
{
	for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter)
		delete *iter;
}

void	Event::AddComment(const std::string& text)
{
	comment = text;
}

void	Event::AddScript(Script *script)
{
	script_list.push_back(script);
}

bool	Event::AddScript(const std::string *ev_params,const std::string *scr_params)
{
	Script	*script = ScriptFactory::GetFactory()->CreateScript(ev_params,scr_params);
	if(script == 0)
		return(false);

	AddScript(script);
	std::ostringstream	buffer;
	script->Write(buffer);
	EvEdit::MainWindow()->ClearScriptDisplay();
	EvEdit::MainWindow()->AddScript(buffer.str());
	if(script_list.size() >1)
	{
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_UP_BTN,true);
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_DOWN_BTN,true);
	}
	else
	{
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_UP_BTN,false);
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_DOWN_BTN,false);
	}
	
	return(true);
}

void	Event::DeleteScript(int which)
{
	int count = 0;
	for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter,++count)
	{
		if(count == which)
		{
			Script *script = *iter;
			script_list.erase(iter);
			delete script;
			break;
		}
	}
		
	std::ostringstream	buffer;
	std::list<std::string>	scr_list;
	for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter)
	{
		buffer.str("");
		(*iter)->Write(buffer);
		scr_list.push_back(buffer.str());
	}
	EvEdit::MainWindow()->DisplayScriptList(scr_list);
	EvEdit::MainWindow()->ClearScriptDisplay();
	if(script_list.size() < 2)
	{
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_UP_BTN,false);
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_DOWN_BTN,false);
	}
}

void	Event::DisplayScript(int which)
{
	Script *script = Find(which);
	if(script == 0) 
		EvEdit::MainWindow()->Message("Unable to find the selected script\nPlease report this problem to ibgames");
	else
		script->Display();
}

void	Event::DisplayScriptList(const std::string& cat_name,const std::string& section_name)
{
	std::ostringstream	buffer;
	std::list<std::string>	scr_list;
	for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter)
	{
		buffer.str("");
		(*iter)->Write(buffer);
		scr_list.push_back(buffer.str());
	}
	EvEdit::MainWindow()->DisplayScriptList(scr_list);

	if(script_list.size() >1)
	{
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_UP_BTN,true);
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_DOWN_BTN,true);
	}
	else
	{
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_UP_BTN,false);
		EvEdit::MainWindow()->SetButtonEnableStatus(EvEdit::MOVE_SCRIPT_DOWN_BTN,false);
	}

	std::string	params[Script::MAX_EVENT_DISPLAY];
	params[Script::CATEGORY] = cat_name;
	params[Script::SECTION] = section_name;
	buffer.str("");
	buffer << number;
	params[Script::NUMBER] = buffer.str();
	params[Script::COMMENT] = comment;
	EvEdit::MainWindow()->DisplayEvent(params);
}

const std::string Event::NumberAndComment()
{
	static std::ostringstream	buffer;
	buffer.str("");
	buffer << number << " (" << comment << ")";
	return(buffer.str());
}

Script	*Event::Find(int index)
{
	ScriptList::iterator iter;
	int which;
	for(which = 0,iter = script_list.begin();iter != script_list.end();++iter,++which)
	{
		if(which == index)
			return(*iter);
	}
	return(0);
}

void	Event::List()
{
	std::ostringstream	buffer;
	buffer << "    Start event: #" << number;
	EvEdit::MainWindow()->List(buffer,"crimson");
	if(comment.length() > 0)
	{
		buffer.str("");
		buffer << "    Comment: " << comment;
		EvEdit::MainWindow()->List(buffer,"grey");
	}
	for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter)
	{
		buffer.str("");
		buffer << "      ";
		(*iter)->Write(buffer);
		EvEdit::MainWindow()->List(buffer);
	}
	buffer.str("");
	buffer << "    End event: #" << number;
	EvEdit::MainWindow()->List(buffer,"crimson");
}

void	Event::MoveScript(int which,int how)
{
	if((how == EvEdit::MOVE_UP) && (which == 0))
		return;
	if((how == EvEdit::MOVE_DOWN) && (which == (script_list.size() - 1)))
		return;

	int count = 0;
	ScriptList::iterator iter, start, finish, from;
	for(iter = script_list.begin();iter != script_list.end();++iter,++count)
	{
		if(count == which)
		{
			if(how == EvEdit::MOVE_DOWN)
				++iter;
			from = iter;
			--iter;
			start = iter;
			finish = ++iter;
			std::swap_ranges(start,finish,from);
			break;
		}
	}

	if(iter != script_list.end())
	{
		std::ostringstream	buffer;
		std::list<std::string>	scr_list;
		for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter)
		{
			buffer.str("");
			(*iter)->Write(buffer);
			scr_list.push_back(buffer.str());
		}
		EvEdit::MainWindow()->DisplayScriptList(scr_list);
	}
}

void	Event::Save(std::ofstream& file)
{
	if(script_list.size() > 0)
	{
		file << "\n      <event num='" << number << "'>\n";

		if(comment != "")
		{
			std::string temp(comment);
			file << "         <comment>" << Script::EscapeXML(Script::CleanText(temp)) << "</comment>\n";
		}
		for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter)
			(*iter)->Save(file);
		file << "      </event>\n";
	}
}

void	Event::UpdateScript(const std::string *ev_params,const std::string *scr_params,int which)
{
	int count = 0;
	for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter,++count)
	{
		if(count == which)
		{
			(*iter)->Update(ev_params,scr_params);
			break;
		}
	}

	std::ostringstream	buffer;
	std::list<std::string>	scr_list;
	for(ScriptList::iterator iter = script_list.begin();iter != script_list.end();++iter)
	{
		buffer.str("");
		(*iter)->Write(buffer);
		scr_list.push_back(buffer.str());
	}
	EvEdit::MainWindow()->DisplayScriptList(scr_list);
}

