/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef SCRIPT_H
#define SCRIPT_H

#include <fstream>
#include <sstream>
#include <string>

#include <QXmlAttributes>

class	Script
{
public:
	enum		// used for setting up the script display
	{
		SCRIPT,		LEVEL, 		LABEL0,		EDIT0,		LABEL1,		EDIT1,
		LABEL2,		EDIT2,		LABEL3,		EDIT3,		LABEL4,		EDIT4,
		LABEL5,		EDIT5,		COMBOLAB0,	COMBO0,		COMBOLAB1,	COMBO1,
		TEXTLAB,		TEXTEDIT,	MAX_SCRIPT_DISPLAY
	};
	enum	{ CATEGORY,	SECTION,		NUMBER,		COMMENT,		MAX_EVENT_DISPLAY	};

protected:
	enum	{ SINGLE, MULTI, TEXT,	};

	static const std::string	mssg_types[];
	static const std::string	planet_owners;
	static const std::string	staff_only;
	
	static const std::string&	MakeFullName(const std::string& cat,const std::string& sect,const std::string& num_text);

	static const int	MAX_TEXT_SIZE = 100;					// max number of chars in an inline message

	static int	FindTextType(const std::string& low,
							const std::string& high,const std::string& text);
	static void	GetAttribute(const QXmlAttributes *attribs,
							const std::string& name,std::string& value);				

public:
	static std::string&	CleanText(std::string& source);
	static std::string&	EscapeXML(std::string& source);

	Script()															{	}
	virtual ~Script();
	
	virtual void	AddText(const std::string& text)		{	}
	virtual void	Display()									= 0;
	virtual void	Save(std::ofstream& file)				= 0;
	virtual void	Update(const std::string *ev_params,const std::string *scr_params) = 0;
	virtual void	Write(std::ostringstream& buffer)	= 0;
};

#endif
