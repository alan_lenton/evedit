/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_money.h"
#include "evedit.h"

CheckMoney::CheckMoney(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"amount",value);
		GetAttribute(attribs,"higher",higher);
		GetAttribute(attribs,"equals",equals);
		GetAttribute(attribs,"lower",lower);
	}
}

CheckMoney::CheckMoney(const std::string *ev_params,const std::string *scr_params)
{
	value = scr_params[EDIT0];
	higher = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
	lower = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT5]);
}

CheckMoney::~CheckMoney()
{

}

void	CheckMoney::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checkmoney";
	display[Script::LEVEL] = planet_owners;

	display[Script::LABEL0] = "Value to test";
	display[Script::EDIT0] = value;
	display[Script::LABEL3] = "Higher";
	display[Script::EDIT3] = higher;
	display[Script::LABEL4] = "Equals";
	display[Script::EDIT4] = equals;
	display[Script::LABEL5] = "Lower";
	display[Script::EDIT5] = lower;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckMoney::Save(std::ofstream& file)
{
	file << "         <checkmoney amount='" << value << "'";
	if(higher != "")
		file << " higher='" << higher << "'";
	if(equals != "")
		file << " equals='" << equals << "'";
	if(lower != "")
		file << " lower='" << lower << "'";
	file << "/>\n";
}

void	CheckMoney::Update(const std::string *ev_params,const std::string *scr_params)
{
	value = scr_params[EDIT0];
	higher = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
	lower = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT5]);
}

bool	CheckMoney::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't told me what\namount to check for!\n");
		return(false);
	}
	if((scr_params[EDIT3] == "") && (scr_params[EDIT4] == "") && (scr_params[EDIT5] == ""))
	{
		EvEdit::MainWindow()->Message("You need to give me at least\none event to call!\n");
		return(false);
	}
	return(true);
}

void	CheckMoney::Write(std::ostringstream& buffer)
{
	buffer << "checkmoney script:  amount " << value;
	if(higher != "")
		buffer << ":  higher " << higher;
	if(equals != "")
		buffer << ":  equals " << equals;
	if(lower != "")
		buffer << ":  lower " << lower;
}

