/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "script_factory.h"

#include "announce.h"
#include "call.h"
#include "change_gender.h"
#include "change_link.h"
#include "change_money.h"
#include "change_player.h"
#include "change_size.h"
#include "change_slithy.h"
#include "change_stat.h"
#include "check4owner.h"
#include "check_flag.h"
#include "check_gender.h"
#include "check_insurance.h"
#include "check_inventory.h"
#include "check_last_loc.h"
#include "check_loc_flags.h"
#include "check_map.h"
#include "check_money.h"
#include "check_player.h"
#include "check_rank.h"
#include "check_size.h"
#include "check_slithy.h"
#include "check_stat.h"
#include "check_variable.h"
#include "clear_variable.h"
#include "create_object.h"
#include "delay_event.h"
#include "destroy_inv.h"
#include "destroy_object.h"
#include "display_size.h"
#include "drop.h"
#include "flip_flag.h"
#include "freeze.h"
#include "get.h"
#include "log.h"
#include	"match.h"
#include "message.h"
#include "move.h"
#include "nomatch.h"
#include "percent.h"
#include "post_to_review.h"
#include "release.h"
#include "set_variable.h"

ScriptFactory		*ScriptFactory::instance = 0;
const int			ScriptFactory::UNKNOWN_SCRIPT = 999;

const std::string	ScriptFactory::names[] = 
{
	"announce", "call", "changegender", "changelink", "changemoney", "changeplayer", 
	"changesize", "changeslithy", "changestat", "checkflag", "checkforowner", 
	"checkgender", "checkinsurance", "checkinventory", "checklastloc", "checklocflag", 
	"checkmap", "checkmoney", "checkplayer", "checkrank", "checksize", "checkslithy", 
	"checkstat", "checkvariable", "clearvariable", "createobject", "delayevent", 
	"destroyinv", "destroyobject", "displaysize", "drop", "flipflag", "freeze", 
	"get", "log", "match", "message", "move", "nomatch", "percent", "posttoreview", 
	"release", "setvariable",
	""
};

ScriptFactory::~ScriptFactory()
{

}


Script	*ScriptFactory::CreateScript(const std::string& which,const QXmlAttributes *attribs)
{
	switch(Find(which))
	{
		case	ANNOUNCE:		return(new Announce(attribs));
		case	CALL:				return(new Call(attribs));
		case	CHG_GENDER:		return(new ChangeGender(attribs));
		case	CHG_LINK:		return(new ChangeLink(attribs));
		case	CHG_MONEY:		return(new ChangeMoney(attribs));
		case	CHG_PLAYER:		return(new ChangePlayer(attribs));
		case	CHG_SIZE:		return(new ChangeSize(attribs));
		case	CHG_SLITHY:		return(new ChangeSlithy(attribs));
		case	CHG_STAT:		return(new ChangeStat(attribs));
		case	CHK_FLAG:		return(new CheckFlag(attribs));
		case	CHK_FOR_OWNER:	return(new CheckForOwner(attribs));
		case	CHK_GENDER:		return(new CheckGender(attribs));
		case	CHK_INSURANCE:	return(new CheckInsurance(attribs));
		case	CHK_INV:			return(new CheckInventory(attribs));
		case	CHK_LAST_LOC:	return(new CheckLastLoc(attribs));
		case	CHK_LOC_FLAGS:	return(new CheckLocFlags(attribs));
		case	CHK_MAP:			return(new CheckMap(attribs));
		case	CHK_MONEY:		return(new CheckMoney(attribs));
		case	CHK_PLAYER:		return(new CheckPlayer(attribs));
		case	CHK_RANK:		return(new CheckRank(attribs));
		case	CHK_SIZE:		return(new CheckSize(attribs));
		case	CHK_SLITHY:		return(new CheckSlithy(attribs));
		case	CHK_STAT:		return(new CheckStat(attribs));
		case	CHK_VARIABLE:	return(new CheckVariable(attribs));
		case	CLR_VARIABLE:	return(new ClearVariable(attribs));
		case	CREATE_OBJ:		return(new CreateObject(attribs));
		case	DELAY:			return(new DelayEvent(attribs));
		case	DESTROY_INV:	return(new DestroyInv(attribs));
		case	DESTROY_OBJ:	return(new DestroyObject(attribs));
		case	DISPLAY_SIZE:	return(new DisplaySize(attribs));
		case	DROP:				return(new Drop(attribs));
		case	FLIP_FLAG:		return(new FlipFlag(attribs));
		case	FREEZE:			return(new Freeze(attribs));
		case	GET_OBJ:			return(new Get(attribs));
		case	LOG:				return(new Log(attribs));
		case	MATCH:			return(new Match(attribs));
		case	MESSAGE:			return(new Message(attribs));
		case	MOVE:				return(new Move(attribs));
		case	NOMATCH:			return(new NoMatch(attribs));
		case	PERCENT:			return(new Percent(attribs));
		case	POST2REVIEW:	return(new PostToReview(attribs));
		case	RELEASE:			return(new Release(attribs));
		case	SET_VAR:			return(new SetVariable(attribs));

		default:		return(0);
	}
}

Script	*ScriptFactory::CreateScript(const std::string *ev_params,const std::string *scr_params)
{
	Script	*script = 0;
	switch(Find(scr_params[Script::SCRIPT]))
	{
		case	 ANNOUNCE:			try { script = new Announce(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 CALL:				try { script = new Call(ev_params,scr_params);				} catch(...) { script = 0;	};	break;
		case	 CHG_GENDER:		try { script = new ChangeGender(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHG_LINK:			try { script = new ChangeLink(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHG_MONEY:			try { script = new ChangeMoney(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHG_PLAYER:		try { script = new ChangePlayer(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHG_SIZE:			try { script = new ChangeSize(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHG_SLITHY:		try { script = new ChangeSlithy(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHG_STAT:			try { script = new ChangeStat(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_FLAG:			try { script = new CheckFlag(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_FOR_OWNER:	try { script = new CheckForOwner(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHK_GENDER:		try { script = new CheckGender(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_INSURANCE:	try { script = new CheckInsurance(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHK_INV:			try { script = new CheckInventory(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHK_LAST_LOC:		try { script = new CheckLastLoc(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHK_LOC_FLAGS:	try { script = new CheckLocFlags(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CHK_MAP:			try { script = new CheckMap(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 CHK_MONEY:			try { script = new CheckMoney(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_PLAYER:		try { script = new CheckPlayer(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_RANK:			try { script = new CheckRank(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_SIZE:			try { script = new CheckSize(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_SLITHY:		try { script = new CheckSlithy(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_STAT:			try { script = new CheckStat(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 CHK_VARIABLE:		try { script = new CheckVariable(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CLR_VARIABLE:		try { script = new ClearVariable(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 CREATE_OBJ:		try { script = new CreateObject(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 DELAY:				try { script = new DelayEvent(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 DESTROY_INV:		try { script = new DestroyInv(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 DESTROY_OBJ:		try { script = new DestroyObject(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 DISPLAY_SIZE:		try { script = new DisplaySize(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
		case	 DROP:				try { script = new Drop(ev_params,scr_params);				} catch(...) { script = 0;	};	break;
		case	 FLIP_FLAG:			try { script = new FlipFlag(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 FREEZE:				try { script = new Freeze(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 GET_OBJ:			try { script = new Get(ev_params,scr_params);				} catch(...) { script = 0;	};	break;
		case	 LOG:					try { script = new Log(ev_params,scr_params);				} catch(...) { script = 0;	};	break;
		case	 MATCH:				try { script = new Match(ev_params,scr_params);				} catch(...) { script = 0;	};	break;
		case	 MESSAGE:			try { script = new Message(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 MOVE:				try { script = new Move(ev_params,scr_params);				} catch(...) { script = 0;	};	break;
		case	 NOMATCH:			try { script = new NoMatch(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 PERCENT:			try { script = new Percent(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 POST2REVIEW:		try { script = new PostToReview(ev_params,scr_params);	} catch(...) { script = 0;	};	break;
		case	 RELEASE:			try { script = new Release(ev_params,scr_params);			} catch(...) { script = 0;	};	break;
		case	 SET_VAR:			try { script = new SetVariable(ev_params,scr_params);		} catch(...) { script = 0;	};	break;
	}
	return(script);
}

bool	ScriptFactory::ValidateScriptParams(const std::string *scr_params)
{
	switch(Find(scr_params[Script::SCRIPT]))
	{
		case	 ANNOUNCE:			return(Announce::Validate(scr_params));
		case	 CALL:				return(Call::Validate(scr_params));
		case	 CHG_MONEY:			return(ChangeMoney::Validate(scr_params));
		case	 CHG_LINK:			return(ChangeLink::Validate(scr_params));
		case	 CHG_GENDER:		return(ChangeGender::Validate(scr_params));
		case	 CHG_PLAYER:		return(ChangePlayer::Validate(scr_params));
		case	 CHG_SIZE:			return(ChangeSize::Validate(scr_params));
		case	 CHG_SLITHY:		return(ChangeSlithy::Validate(scr_params));
		case	 CHG_STAT:			return(ChangeStat::Validate(scr_params));
		case	 CHK_FOR_OWNER:	return(CheckForOwner::Validate(scr_params));
		case	 CHK_FLAG:			return(CheckFlag::Validate(scr_params));
		case	 CHK_GENDER:		return(CheckGender::Validate(scr_params));
		case	 CHK_INSURANCE:	return(CheckInsurance::Validate(scr_params));
		case	 CHK_INV:			return(CheckInventory::Validate(scr_params));
		case	 CHK_LAST_LOC:		return(CheckLastLoc::Validate(scr_params));
		case	 CHK_LOC_FLAGS:	return(CheckLocFlags::Validate(scr_params));
		case	 CHK_MAP:			return(CheckMap::Validate(scr_params));
		case	 CHK_MONEY:			return(CheckMoney::Validate(scr_params));
		case	 CHK_PLAYER:		return(CheckPlayer::Validate(scr_params));
		case	 CHK_RANK:			return(CheckRank::Validate(scr_params));
		case	 CHK_SIZE:			return(CheckSize::Validate(scr_params));
		case	 CHK_SLITHY:		return(CheckSlithy::Validate(scr_params));
		case	 CHK_STAT:			return(CheckStat::Validate(scr_params));
		case	 CHK_VARIABLE:		return(CheckVariable::Validate(scr_params));
		case	 CLR_VARIABLE:		return(ClearVariable::Validate(scr_params));
		case	 CREATE_OBJ:		return(CreateObject::Validate(scr_params));
		case	 DELAY:				return(DelayEvent::Validate(scr_params));
		case	 DESTROY_INV:		return(DestroyInv::Validate(scr_params));
		case	 DESTROY_OBJ:		return(DestroyObject::Validate(scr_params));
		case	 DISPLAY_SIZE:		return(DisplaySize::Validate(scr_params));
		case	 DROP:				return(Drop::Validate(scr_params));
		case	 FLIP_FLAG:			return(FlipFlag::Validate(scr_params));
		case	 FREEZE:				return(Freeze::Validate(scr_params));
		case	 GET_OBJ:			return(Get::Validate(scr_params));
		case	 LOG:					return(Log::Validate(scr_params));
		case	 MATCH:				return(Match::Validate(scr_params));
		case	 MESSAGE:			return(Message::Validate(scr_params));
		case	 MOVE:				return(Move::Validate(scr_params));
		case	 NOMATCH:			return(NoMatch::Validate(scr_params));
		case	 PERCENT:			return(Percent::Validate(scr_params));
		case	 POST2REVIEW:		return(PostToReview::Validate(scr_params));
		case	 RELEASE:			return(Release::Validate(scr_params));
		case	 SET_VAR:			return(SetVariable::Validate(scr_params));

		default:		return(false);
	}
}

void	ScriptFactory::DestroyFactory()
{
	delete instance;
	instance = 0;
}

/********* add try/catch block when reporting sorted out ********/
ScriptFactory	*ScriptFactory::GetFactory()
{
	if(instance == 0)
		instance = new ScriptFactory;
	return(instance);
}

int	ScriptFactory::Find(const std::string& name)	const
{
	for(int count = 0;names[count] != "";++count)
	{
		if(names[count] == name)
			return(count);
	}
	return(UNKNOWN_SCRIPT);
}

void	ScriptFactory::GetComboUpdateInfo(int which_changed,const std::string text,const std::string script_name,std::string *scr_params)
{
	switch(Find(script_name))
	{
		case	CHG_LINK:	ChangeLink::GetComboUpdateInfo(which_changed,text,scr_params);		break;
		case	CREATE_OBJ:	CreateObject::GetComboUpdateInfo(which_changed,text,scr_params);	break;
		case	MOVE:			Move::GetComboUpdateInfo(which_changed,text,scr_params);				break;
	}
}


