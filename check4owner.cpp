/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check4owner.h"

#include <cctype>

#include "evedit.h"

CheckForOwner::CheckForOwner(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"owner",owner);
		GetAttribute(attribs,"visitor",visitor);
	}
}

CheckForOwner::CheckForOwner(const std::string *ev_params,const std::string *scr_params)
{
	owner = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	visitor = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

CheckForOwner::~CheckForOwner()
{

}


void	CheckForOwner::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checkforowner";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Owner";
	display[Script::EDIT0] = owner;
	display[Script::LABEL1] = "Visitor";
	display[Script::EDIT1] = visitor;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckForOwner::Save(std::ofstream& file)
{
	file << "         <checkforowner owner='" << owner << "'";
	if(visitor != "")
		file << " visitor='" << visitor << "'";
	file << "/>\n";
}

void	CheckForOwner::Update(const std::string *ev_params,const std::string *scr_params)
{
	owner = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	visitor = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

bool	CheckForOwner::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == ""))
	{
		EvEdit::MainWindow()->Message("You must give me an event to call for\neither the owner or the visitor!\n");
		return(false);
	}
	return(true);
}

void	CheckForOwner::Write(std::ostringstream& buffer)
{
	buffer << "checkforowner script";
	if(owner != "")
		buffer << ":  owner event " << owner;
	if(visitor!= "")
		buffer << ":  visitor event " << visitor;
}

