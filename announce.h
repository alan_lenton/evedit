/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef ANNOUNCE_H
#define ANNOUNCE_H

#include	<string>

#include "script.h"

class Announce : public Script
{
private:
	std::string	high;
	std::string	low;
	std::string	text;
	std::string	loc_num;
	
	Announce(const Announce& rhs);
	Announce& operator=(const Announce& rhs);

public:
	static bool	Validate(const std::string *scr_params);

	Announce(const QXmlAttributes *attribs = 0);
	Announce(const std::string *ev_params,const std::string *scr_params);
	~Announce();

	void	AddText(const std::string& new_text);
	void	Display();
	void	Save(std::ofstream& file);
	void	Update(const std::string *ev_params,const std::string *scr_params);
	void	Write(std::ostringstream& buffer);
};

#endif
