/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include	<string>
#include "script.h"

class Message : public Script
{
private:
	static const int	MAX_COMBO_0;

	std::string	who;
	std::string	low;
	std::string	high;
	std::string	text;
	
	Message(const Message& rhs);
	Message& operator=(const Message& rhs);
	
	int FindComboIndex(const std::string& which) const;

public:
	static bool	Validate(const std::string *scr_params);

	Message(const QXmlAttributes *attribs = 0);
	Message(const std::string *ev_params,const std::string *scr_params);
	~Message();

	void	AddText(const std::string& new_text);
	void	Display();
	void	Save(std::ofstream& file);
	void	Update(const std::string *ev_params,const std::string *scr_params);
	void	Write(std::ostringstream& buffer);
};

#endif
