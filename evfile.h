/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef EVFILE_H
#define EVFILE_H

#include <map>
#include <string>

#include <QXmlSimpleReader>


class	Category;
class	Event;
class	Script;
class	Section;

typedef	std::map<const std::string,Category *,std::less<std::string> >	CatIndex;

class	EvFile : public QXmlDefaultHandler
{
private:
	static const std::string	el_names[];
	static const int				NOT_FOUND;

	int			version;
	std::string	file_name;
	Category		*current;
	Event			*current_event;	// used to parse input files
	Script		*current_script;	// use to parse input files
	bool			changed;
	CatIndex		index;
	QString		char_data;
	
	EvFile(const EvFile& rhs);
	EvFile& operator=(const EvFile& rhs);

	Category	*Find(const std::string& cat_name);

	int	FindElement(const std::string& element);

	// These three functions are part of Qt's XML parsing operation	
	bool	characters(const QString& text);
	bool	endElement(const QString& namespaceURI,const QString& localName,const QString& qName);
	bool	startElement(const QString& namespaceURI,const QString& localName,
												const QString& qName, const QXmlAttributes& atts);

	bool	EventExists(const std::string *ev_params);
	bool	IsCurrentEvent(const std::string *ev_params);
	bool	ValidateEvent(const std::string *ev_params);

	void	EndParsing();
	void	EndScript();
	void	StartCategory(const QXmlAttributes& attribs);
	void	StartComment(const QXmlAttributes& attribs);
	void	StartEvent(const QXmlAttributes& attribs);
	void	StartEventList(const QXmlAttributes& attribs);
	void	StartScript(const std::string& script_name,const QXmlAttributes& attribs);
	void	StartSection(const QXmlAttributes& attribs);

public:
	EvFile	(const std::string& f_name = "",const int ver_num = 1) : 
								version(ver_num), file_name(f_name), current(0), 
								current_event(0), current_script(0), changed(false)	{	}
	virtual ~EvFile();

	int	NextFreeEvNumber();
	int	NextFreeEvNumber(std::string *ev_params);
	int	NumScriptsInCurEvent();

	bool	CanClose();
	bool	Changed()									{ return(changed);		}
	bool	HasCurrentCategory()						{ return(current != 0);	}
	bool	NewScript(const std::string& script_name);
	bool	ScriptSelected(int which);

	void	AddCategory(Category *category);
	void	AddSection(Section *section);
	void	AddEvent(Event *event);
	void	AddScript(const std::string *ev_params,const std::string *scr_params);
	void	CategorySelected(std::string& cat_name);
	void	Clear();
	void	ClearFileName();
	void	DeleteCurrentCategory();
	void	DeleteCurrentEvent();
	void	DeleteCurrentSection();
	void	DeleteScript(int which);
	void	DisplayCategoryList();
	void	EventSelected(std::string& event_number);
	void	FileName(std::string& new_name)		{ file_name = new_name;	}
	void	GetComboUpdateInfo(int which_changed,const std::string text,const std::string script_name,std::string *scr_params);
	void	List();
	void	Load();
	void	MoveScript(int which,int how);
	void	NewEvent(std::string *ev_params);
	void	NextEvent(std::string *ev_params);
	void	Save();
	void	SectionSelected(std::string& section_name);
	void	SetChangedFlag(bool new_value)		{ changed = new_value;	}
	void	UpdateComment(const std::string& text);
	void	UpdateScript(const std::string *ev_params,const std::string *scr_params,int which);						
	void	ResetVersion()								{ version = 1;				}
};

#endif

