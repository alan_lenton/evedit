/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_Size.h"
#include "evedit.h"

CheckSize::CheckSize(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"id-name",id_name);
		GetAttribute(attribs,"value",value);

		GetAttribute(attribs,"more",more);
		GetAttribute(attribs,"equal",equal);
		GetAttribute(attribs,"less",less);
	}
}

CheckSize::CheckSize(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	value = scr_params[EDIT1];

	more = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
	equal = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	less = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

CheckSize::~CheckSize()
{

}


void	CheckSize::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checksize";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "ID or name";
	display[Script::EDIT0] = id_name;
	display[Script::LABEL1] = "Value to check";
	display[Script::EDIT1] = value;

	display[Script::LABEL2] = "Higher";
	display[Script::EDIT2] = more;
	display[Script::LABEL3] = "Equals";
	display[Script::EDIT3] = equal;
	display[Script::LABEL4] = "Lower";
	display[Script::EDIT4] = less;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckSize::Save(std::ofstream& file)
{
	file << "         <checksize";
	file << " id-name='" << id_name << "'";
	file << " value='"   << value   << "'";

	if(more != "")		file << " more='"  << more  << "'";
	if(equal != "")	file << " equal='" << equal << "'";
	if(less != "")		file << " less='"  << less  << "'";

	file <<"/>\n";
}

void	CheckSize::Update(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	value   = scr_params[EDIT1];

	more  = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
	equal = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	less  = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

bool	CheckSize::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") || (scr_params[EDIT1] == ""))
	{
		EvEdit::MainWindow()->Message("I need an object name or ID and a size value to check!");
		return(false);
	}

	if((scr_params[EDIT2] == "") && (scr_params[EDIT3] == "") && (scr_params[EDIT4] == ""))
	{
		EvEdit::MainWindow()->Message("I need at least one event to call!");
		return(false);
	}
	return(true);
}

void	CheckSize::Write(std::ostringstream& buffer)
{
	buffer << "checksize script";

	buffer << ": id-name "	<< id_name;
	buffer << ": value "		<< value;

	if(more != "")		buffer << ": higher "	<< more;
	if(equal != "")	buffer << ": equals "	<< equal;
	if(less != "")		buffer << ": less "		<< less;
}



