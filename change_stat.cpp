/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "change_stat.h"
#include "evedit.h"

#include <cctype>

ChangeStat::ChangeStat(const QXmlAttributes *attribs)
{
	SetUpCombo0();
	if(attribs != 0)
	{
		GetAttribute(attribs,"who",who);
		std::string	temp;
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			GetAttribute(attribs,combo_0[count],temp);
			if(temp != "")
			{
				stat = combo_0[count];
				value = temp;
				break;
			}
		}

		GetAttribute(attribs,"change",change);

		GetAttribute(attribs,"cur-max",cur_max);
	}
}

ChangeStat::ChangeStat(const std::string *ev_params,const std::string *scr_params)
{
	SetUpCombo0();
	stat = scr_params[COMBO0];
	who = scr_params[COMBO1];
	value = scr_params[EDIT0];
	change = scr_params[EDIT1];
	cur_max = scr_params[EDIT2];
}

ChangeStat::~ChangeStat()
{

}


void ChangeStat::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];

	display[SCRIPT] = "changestat";
	display[LEVEL] = staff_only;

	display[LABEL0] = "Amount";
	display[EDIT0] = value;
	display[Script::LABEL1] = "Add/Set value?";
	display[Script::EDIT1] = change;
	if(change == "")
		display[EDIT1] = "add";
	display[Script::LABEL2] = "Current/Max?";
	display[Script::EDIT2] = cur_max;
	if(cur_max == "")
		display[EDIT2] = "current";

	if(stat != "")
	{
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			if(stat == combo_0[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_0[1] = buffer.str();
				break;
			}
		}
	}
	else
		combo_0[1] = "0";

	std::string combo_1[MAX_COMBO_1];
	combo_1[0] = "Who to change";
	combo_1[1] = "0";
	combo_1[2] = "individual";
	combo_1[3] = "room";
	combo_1[4] = "room-ex";
	combo_1[5] = "";

	if(who != "")
	{
		for(int count = 2; count < MAX_COMBO_1 - 1;++count)
		{
			if(who == combo_1[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_1[1] = buffer.str();
				break;
			}
		}
	}
	EvEdit::MainWindow()->DisplayScript(display,combo_0,combo_1);
}

void	ChangeStat::Save(std::ofstream& file)
{
	file << "         <changestat who='" << who << "'";
	file << " " << stat << "='" << value << "'";
	file << " change='" << change << "'";
	file << " cur-max='" << cur_max << "'";
	file << "/>\n";
}

void	ChangeStat::SetUpCombo0()
{
	combo_0[0]  = "Stat name";
	combo_0[1]  = "0";
	combo_0[2]  = "strength";
	combo_0[3]  = "stamina";
	combo_0[4]  = "dexterity";
	combo_0[5]  = "intelligence";
	combo_0[6]  = "money";
	combo_0[7]  = "reward";
	combo_0[8]  = "ak";
	combo_0[9]  = "tc";
	combo_0[10] = "total";
	combo_0[11] = "rank";
	combo_0[12] = "c-money";
	combo_0[13] = "customs";
	combo_0[14] = "killed";
	combo_0[15] = "remote-check";
	combo_0[16] = "shares";
	combo_0[17] = "treasury";
	combo_0[18] = "hc";
	combo_0[19] = "";
}

void	ChangeStat::Update(const std::string *ev_params, const std::string *scr_params)
{
	stat = scr_params[COMBO0];
	who = scr_params[COMBO1];
	value = scr_params[EDIT0];
	change = scr_params[EDIT1];
	cur_max = scr_params[EDIT2];
}

bool	ChangeStat::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("I need an amount to change the stat by!");
		return(false);
	}

	std::string	temp = scr_params[EDIT1];
	for(int count = 0;count < temp.length();++count)
		temp[count] = tolower(temp[count]);
	if((temp != "add") && (temp != "set"))
	{
		EvEdit::MainWindow()->Message("Please enter either 'add' or 'set'\nin the 'Add/Set value box.");
		return false;
	}

	temp = scr_params[EDIT2];
	for(int count = 0;count < temp.length();++count)
		temp[count] = tolower(temp[count]);
	if((temp != "current") && (temp != "max"))
	{
		EvEdit::MainWindow()->Message("Please enter either 'current' or 'max'\nin the 'Current/Max value box.");
		return false;
	}

	return(true);
}

void	ChangeStat::Write(std::ostringstream& buffer)
{
	buffer << "changestat script:  who " << who;
	buffer << ":  " << stat << " " << value;
	buffer << ":  " << "change " << change;
	buffer << ":  " << "which " << cur_max;
}

