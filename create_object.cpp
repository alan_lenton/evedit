/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "create_object.h"

#include "evedit.h"


const int	CreateObject::MAX_COMBO_0 = 5;

CreateObject::CreateObject(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"home-map",home_map);
		GetAttribute(attribs,"id",id);
		GetAttribute(attribs,"where",where);
		GetAttribute(attribs,"dest-map",dest_map);
		GetAttribute(attribs,"dest-loc",dest_loc);
	}
}

CreateObject::CreateObject(const std::string *ev_params,const std::string *scr_params)
{
	home_map = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	id = scr_params[EDIT1];
	where = scr_params[COMBO0];
	if(where == "room")
	{
		dest_map = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
		dest_loc = scr_params[EDIT4];
	}
	else
		dest_map = dest_loc = "";
}

CreateObject::~CreateObject()
{

}


void	CreateObject::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];

	display[SCRIPT] = "createobject";
	display[LEVEL] = planet_owners;

	display[LABEL0] = "Home Map";
	if(home_map != "")
		display[EDIT0] = home_map;
	else
		display[EDIT0] = "home";
	display[LABEL1] = "Object ID";
	display[EDIT1] = id;
	
	// Change MAX_COMBO_0 if you add to this!
	std::ostringstream buffer;
	buffer << ((where == "room") ? "1" : "0");
	std::string	combo_0[MAX_COMBO_0];
	combo_0[0] = "Where";
	combo_0[1] = buffer.str();
	combo_0[2] = "inventory";
	combo_0[3] = "room";
	combo_0[4] = "";

	if(where == "room")
	{
		display[LABEL3] = "Dest Map";
		display[LABEL4] = "Dest Loc #";
		if(dest_map != "")
			display[EDIT3] = dest_map;
		else
			display[EDIT3] = "current";
		display[EDIT4] = dest_loc;
	}
	
	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	CreateObject::GetComboUpdateInfo(int which_changed,const std::string text,std::string *scr_params)
{
	if(text == "room")
	{
		scr_params[LABEL3] = "Dest Map";
		scr_params[LABEL4] = "Dest Loc #";
		if(scr_params[EDIT3] == "")
			scr_params[EDIT3] = "current";
	}

	if(text == "inventory")
	{
		scr_params[Script::LABEL3] = "~disable";
		scr_params[Script::LABEL4] = "~disable";
	}
}


void	CreateObject::Save(std::ofstream& file)
{
	file << "         <createobject where='" << where << "'";
	file << " home-map='" << home_map << "'";
	file << " id='" << id << "'";
	if(where == "room")
	{
		file << " dest-map='" << dest_map << "'";
		file << " dest-loc='" << dest_loc << "'"; 
	}
	file << "/>\n";
}

void	CreateObject::Update(const std::string *ev_params,const std::string *scr_params)
{
	where = scr_params[COMBO0];
	home_map = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	id = scr_params[EDIT1];
	if(where == "room")
	{
		dest_map = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
		dest_loc = scr_params[EDIT4];
	}
	else
		dest_map = dest_loc = "";
}

bool	CreateObject::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a home map!");
		return(false);
	}
	if(scr_params[EDIT1] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me an object ID!");
		return(false);
	}

	if(scr_params[COMBO0] == "room")
	{
		if(scr_params[EDIT3] == "")
		{
			EvEdit::MainWindow()->Message("You haven't given me a destination map!");
			return(false);
		}
		if(scr_params[EDIT4] == "")
		{
			EvEdit::MainWindow()->Message("You haven't given me a\ndestination location number!");
			return(false);
		}
	}

	return(true);
}

void	CreateObject::Write(std::ostringstream& buffer)
{
	buffer << "createobject script:  where " << where;
	buffer << ":  home-map " << home_map << ":  id " << id;
	if(where == "room")
		buffer << ":  dest-map " << dest_map << ":  dest-loc " << dest_loc; 
}

