/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_variable.h"
#include "evedit.h"


CheckVariable::CheckVariable(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"name",name);
		GetAttribute(attribs,"key",key);
		GetAttribute(attribs,"value",value);
		GetAttribute(attribs,"equals",equals);
		GetAttribute(attribs,"not-equal",not_equal);
	}
}

CheckVariable::CheckVariable(const std::string *ev_params,const std::string *scr_params)
{
	name = scr_params[EDIT0];
	key = scr_params[EDIT1];
	value = scr_params[EDIT2];
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	not_equal = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

CheckVariable::~CheckVariable()
{

}


void	CheckVariable::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "checkvariable";
	display[Script::LEVEL] = planet_owners;

	display[Script::LABEL0] = "Name";
	display[Script::EDIT0] = name;
	display[Script::LABEL1] = "Key";
	display[Script::EDIT1] = key;
	display[Script::LABEL2] = "Value";
	display[Script::EDIT2] = value;
	display[Script::LABEL3] = "Equals";
	display[Script::EDIT3] = equals;
	display[Script::LABEL4] = "Not equal";
	display[Script::EDIT4] = not_equal;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckVariable::Save(std::ofstream& file)
{
	file << "         <checkvariable name='" << name << "'";
	file << " key='" << key << "'";
	file << " value='" << value << "'";
	if(equals != "")		file << " equals='" << equals << "'";
	if(not_equal != "")	file << " not-equal='" << not_equal << "'";
	file << "/>\n";
}

void	CheckVariable::Update(const std::string *ev_params,const std::string *scr_params)
{
	name = scr_params[EDIT0];
	key = scr_params[EDIT1];
	value = scr_params[EDIT2];
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	not_equal = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

bool	CheckVariable::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a name!");
		return(false);
	}

	if(scr_params[EDIT1] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a key!");
		return(false);
	}

	if(scr_params[EDIT2] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a value!");
		return(false);
	}

	if((scr_params[EDIT3] == "") && (scr_params[EDIT4] == ""))
	{
		EvEdit::MainWindow()->Message("You need to give me at least one event!");
		return(false);
	}

	return(true);
}

void	CheckVariable::Write(std::ostringstream& buffer)
{
	buffer << "checkvariable script:  name " << name;
	buffer << ":  key " << key;
	buffer << ":  value " << value;
	if(equals != "")
		buffer << ":  equals " << equals;
	if(not_equal != "")
		buffer << ":  not-equal " << not_equal;
}



