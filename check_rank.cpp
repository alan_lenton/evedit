/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_rank.h"

#include "evedit.h"

const int   CheckRank::MAX_COMBO_0 = 19;

CheckRank::CheckRank(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"level",level);
		GetAttribute(attribs,"higher",higher);
		GetAttribute(attribs,"equals",equals);
		GetAttribute(attribs,"lower",lower);
	}
}

CheckRank::CheckRank(const std::string *ev_params,const std::string *scr_params)
{
	level = scr_params[COMBO0];
	higher = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	lower = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

CheckRank::~CheckRank()
{

}

void	CheckRank::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checkrank";
	display[Script::LEVEL] = planet_owners;

	display[Script::LABEL0] = "Higher";
	display[Script::EDIT0] = higher;
	display[Script::LABEL1] = "Equals";
	display[Script::EDIT1] = equals;
	display[Script::LABEL2] = "Lower";
	display[Script::EDIT2] = lower;

	std::string combo_0[MAX_COMBO_0];
	combo_0[0]  = "Rank to test";
	combo_0[1]  = "0";
	combo_0[2]  = "groundhog";
	combo_0[3]  = "commander";
	combo_0[4]  = "captain";
	combo_0[5]  = "adventurer";
	combo_0[6]  = "merchant";
	combo_0[7]  = "trader";
	combo_0[8]  = "industrialist";
	combo_0[9]  = "manufacturer";
	combo_0[10] = "financier";
	combo_0[11] = "founder";
	combo_0[12] = "engineer";
	combo_0[13] = "mogul";
	combo_0[14] = "technocrat";
	combo_0[15] = "gengineer";
	combo_0[16] = "magnate";
	combo_0[17] = "plutocrat";
	combo_0[18] = "";

	if(level != "")
	{
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			if(level == combo_0[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_0[1] = buffer.str();
				break;
			}
		}
	}

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	CheckRank::Save(std::ofstream& file)
{
	file << "         <checkrank level='" << level << "'";
	if(higher != "")
		file << " higher='" << higher << "'";
	if(equals != "")
		file << " equals='" << equals << "'";
	if(lower != "")
		file << " lower='" << lower << "'";
	file << "/>\n";
}

void	CheckRank::Update(const std::string *ev_params,const std::string *scr_params)
{
	level = scr_params[COMBO0];
	higher = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	equals = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	lower = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

bool	CheckRank::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == "") && (scr_params[EDIT2] == ""))
	{
		EvEdit::MainWindow()->Message("You need to give me at least\none event to call!\n");
		return(false);
	}
	return(true);
}

void	CheckRank::Write(std::ostringstream& buffer)
{
	buffer << "checkrank script:  rank " << level;
	if(higher != "")
		buffer << ":  higher " << higher;
	if(equals != "")
		buffer << ":  equals " << equals;
	if(lower != "")
		buffer << ":  lower " << lower;
}

