/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef SCRIPTFACTORY_H
#define SCRIPTFACTORY_H

#include <string>
#include <QXmlAttributes>

class	Script;

class ScriptFactory
{
public:
	static const std::string	names[];

	enum					// script types
	{
		ANNOUNCE, CALL, CHG_GENDER, CHG_LINK, CHG_MONEY, CHG_PLAYER, CHG_SIZE, 
		CHG_SLITHY, CHG_STAT, CHK_FLAG, CHK_FOR_OWNER, CHK_GENDER, CHK_INSURANCE, 
		CHK_INV, CHK_LAST_LOC, CHK_LOC_FLAGS, CHK_MAP, CHK_MONEY, CHK_PLAYER, 
		CHK_RANK, CHK_SIZE, CHK_SLITHY, CHK_STAT, CHK_VARIABLE, CLR_VARIABLE, 
		CREATE_OBJ, DELAY, DESTROY_INV, DESTROY_OBJ, DISPLAY_SIZE, DROP, 
		FLIP_FLAG, FREEZE, GET_OBJ, LOG, MATCH, MESSAGE, MOVE, NOMATCH, PERCENT, 
		POST2REVIEW, RELEASE, SET_VAR
	};


private:
	static ScriptFactory			*instance;
	static const int				UNKNOWN_SCRIPT;
	
	ScriptFactory()				{	}
	virtual ~ScriptFactory();
	
	int	Find(const std::string& name)	const;

public:
	static ScriptFactory	*GetFactory();
	static void				DestroyFactory();
	
	Script	*CreateScript(const std::string& which,const QXmlAttributes *attribs = 0);
	Script	*CreateScript(const std::string *ev_params,const std::string *scr_params);
	
	bool		ValidateScriptParams(const std::string *scr_params);

	void		GetComboUpdateInfo(int which_changed,const std::string text,const std::string script_name,std::string *scr_params);
};

#endif
