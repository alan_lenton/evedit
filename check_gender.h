/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef CHECKGENDER_H
#define CHECKGENDER_H

#include	<string>
#include "script.h"

class CheckGender : public Script
{
private:
	std::string	male;
	std::string	female;
	std::string	neuter;

	CheckGender(const CheckGender& rhs);
	CheckGender& operator=(const CheckGender& rhs);

public:
	static bool	Validate(const std::string *scr_params);

	CheckGender(const QXmlAttributes *attribs = 0);
	CheckGender(const std::string *ev_params,const std::string *scr_params);
	~CheckGender();

	void	Display();
	void	Save(std::ofstream& file);
	void	Update(const std::string *ev_params,const std::string *scr_params);
	void	Write(std::ostringstream& buffer);
};

#endif
