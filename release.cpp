/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "release.h"
#include "evedit.h"


Release::~Release()
{
	// no inline virtual destructors
}


void	Release::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "release";
	EvEdit::MainWindow()->DisplayScript(display);
}

void	Release::Save(std::ofstream& file)
{
	file << "         <release/>\n";
}

void	Release::Write(std::ostringstream& buffer)
{
	buffer << "release script";
}

