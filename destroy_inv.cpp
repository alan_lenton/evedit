/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "destroy_inv.h"

#include <cctype>

#include "evedit.h"


DestroyInv::DestroyInv(const QXmlAttributes *attribs)
{
	if(attribs != 0)
		GetAttribute(attribs,"title",map_name);
}

DestroyInv::DestroyInv(const std::string *ev_params,const std::string *scr_params)
{
	map_name = scr_params[EDIT0];
}

DestroyInv::~DestroyInv()
{

}


void	DestroyInv::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];
	display[SCRIPT] = "destroyinv";
	display[LEVEL] = staff_only;
	
	display[LABEL0] = "Map name";
	display[EDIT0] = map_name;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	DestroyInv::Save(std::ofstream& file)
{
	file << "         <destroyinv title='" << map_name << "'/>\n";
}

void	DestroyInv::Update(const std::string *ev_params,const std::string *scr_params)
{
	map_name = scr_params[EDIT0];
}

bool	DestroyInv::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't said which map you want!\n");
		return(false);
	}
	else
		return(true);
}

void	DestroyInv::Write(std::ostringstream& buffer)
{
	buffer << "destroyinv script:  map-name " << map_name;
}

