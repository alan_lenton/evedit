/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_loc_flags.h"
#include "evedit.h"

const int   CheckLocFlags::MAX_COMBO_0 = 12;

CheckLocFlags::CheckLocFlags(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"flag",flag);
		GetAttribute(attribs,"set",set);
		GetAttribute(attribs,"unset",unset);
	}
}

CheckLocFlags::CheckLocFlags(const std::string *ev_params,const std::string *scr_params)
{
	flag = scr_params[COMBO0];
	set = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	unset = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

CheckLocFlags::~CheckLocFlags()
{

}


void CheckLocFlags::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];

	display[SCRIPT] = "checklocflag";
	display[LEVEL] = planet_owners;

	display[LABEL0] = "Flag Set";
	display[EDIT0] = set;
	display[LABEL1] = "Flag Not Set";
	display[EDIT1] = unset;
	
	std::string combo_0[MAX_COMBO_0];
	combo_0[0] = "Flag";
	combo_0[1] = "0";
	combo_0[2] = "bar";
	combo_0[3] = "space";
	combo_0[4] = "link";
	combo_0[5] = "exchange";
	combo_0[6] = "shipyard";
	combo_0[7] = "repair";
	combo_0[8] = "hospital";
	combo_0[9] = "peace";
	combo_0[10] = "custom";
	combo_0[11] = "";

	if(flag != "")
	{
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			if(flag == combo_0[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_0[1] = buffer.str();
				break;
			}
		}
	}

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	CheckLocFlags::Save(std::ofstream& file)
{
	file << "         <checklocflag flag='" << flag << "'";
	if(set != "")
		file << " set='" << set << "'";
	if(unset != "")
		file << " unset= '" << unset << "'";
	file << "/>\n";
}

void CheckLocFlags::Update(const std::string *ev_params, const std::string *scr_params)
{
	flag = scr_params[COMBO0];
	set = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	unset = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

bool CheckLocFlags::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == ""))
	{
		EvEdit::MainWindow()->Message("You haven't given me any events to call!");
		return(false);
	}
	else
		return true;
}

void CheckLocFlags::Write(std::ostringstream& buffer)
{
	buffer << "checklocflag script:  flag " << flag;
	if(set != "")
		buffer << ":  set " << set;
	if(unset != "")
		buffer << ":  unset " << unset;
}

