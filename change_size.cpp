/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "change_size.h"
#include "evedit.h"

const int   ChangeSize::MAX_COMBO_0 = 5;

ChangeSize::ChangeSize(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"id-name",id_name);
		GetAttribute(attribs,"amount",amount);
		GetAttribute(attribs,"change",change);
		if(change == "Add")
			change = "Add to size";
		else
			change = "Set size to";
	}
}

ChangeSize::ChangeSize(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	amount = scr_params[EDIT1];
	change = scr_params[COMBO0];
}

ChangeSize::~ChangeSize()
{

}


void	ChangeSize::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "changesize";
	display[Script::LEVEL] = planet_owners;

	display[LABEL0] = "ID or name";
	display[EDIT0]  = id_name;
	display[LABEL1] = "Amount";
	display[EDIT1]  = amount;

	std::string combo_0[MAX_COMBO_0];
	combo_0[0] = "How to change";
	combo_0[1] = "0";
	combo_0[2] = "Add to size";
	combo_0[3] = "Set size to";
	combo_0[4] = "";

	if(change.find("Set") == 0)
		combo_0[1] = "1";

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	ChangeSize::Save(std::ofstream& file)
{
	file << "         <changesize id-name='" << id_name << "'";
	file << " amount='" << amount << "'";
	if(change.find("Add") == 0)		
		file << " change='add'/>\n";
	else
		file << " change='set'/>\n";
}

void	ChangeSize::Update(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	amount = scr_params[EDIT1];
	change = scr_params[COMBO0];
}

bool	ChangeSize::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a name or ID!");
		return(false);
	}

	if(scr_params[EDIT1] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me an amount for the change!");
		return(false);
	}

	return(true);
}

void	ChangeSize::Write(std::ostringstream& buffer)
{
	buffer << "changesize script:  id-name " << id_name;
	buffer << ": amount " << amount;
	buffer << ": change using " << change;
}



