/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef EVEDIT_H
#define EVEDIT_H

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QString>
#include <QToolBar>

#include <string>

#include "ui_evedit.h"


class	EvFile;

class EvEdit : public QMainWindow
{
	Q_OBJECT

public:
	enum	// Buttons
	{
		ADD_SCRIPT_BTN,	UPDATE_SCRIPT_BTN,	CLEAR_SCRIPT_BTN,				NEW_EVENT_BTN,
		NEXT_EVENT_BTN,	MOVE_SCRIPT_UP_BTN,	MOVE_SCRIPT_DOWN_BTN,		MAX_BTNS
	};

	enum	{ MOVE_UP, MOVE_DOWN	};

private:
	static EvEdit	*main_window;	

	Ui::EvEditClass ui;
	
	std::string		home;

	std::string	version;
	QString		directory;
	EvFile		*ev_file;
	bool			new_script;
	bool			changed;

	QAction	*about_action;
	QAction	*exit_action;
	QAction	*copy_event_action;
	QAction	*del_cat_action;
	QAction	*del_event_action;
	QAction	*del_script_action;
	QAction	*del_section_action;
	QAction	*font_action;
	QAction	*help_action;
	QAction	*load_action;
	QAction	*new_action;
	QAction	*paste_event_action;
	QAction	*save_action;
	QAction	*saveas_action;
	QAction	*test_action;

	QMenu		*help_menu;
	QMenu		*file_menu;
	QMenu		*edit_menu;
	QMenu		*tools_menu;
	QToolBar	*file_toolbar;

	QImage	picture;
	QLabel	*status_label;

	bool	eventFilter(QObject *target,QEvent *event);

	void	closeEvent(QCloseEvent *event);
	void	CreateActions();
	void	CreateMainMenu();
	void	CreateStatusBar();
	void	SetUpConnections();
	void	SetUpScriptNames();

private slots:
	void	OnAddScriptBtnClicked(bool checked);
	void	OnCategoryListClicked(QListWidgetItem *item);
	void	OnClearScriptBtnClicked(bool checked);
 	void	OnCombo0Changed(const QString& text);
 	void	OnCombo1Changed(const QString& text);
	void	OnCommentEditFinished();
	void	OnEventsListClicked(QListWidgetItem *item);
	void	OnLineEditChanged(const QString& text);
	void	OnNewEventBtnClicked(bool checked);
	void	OnNextEventBtnClicked(bool checked);
	void	OnNotebookPageChanged(int index);
	void	OnScriptDownBtnClicked(bool checked);
	void  OnScriptListClicked(QListWidgetItem *item);
	void	OnScriptNamesClicked(QListWidgetItem *item);
	void	OnScriptNamesDoubleClicked(QListWidgetItem *item);
	void	OnScriptUpBtnClicked(bool checked);
	void	OnScriptUpdateBtnClicked(bool checked);
	void  OnSectionListClicked(QListWidgetItem *item);
	void	OnTextChanged();

	void	About();
	void	Close();
	void	Copy();
	void	DeleteCategory();
	void	DeleteEvent();
	void	DeleteScript();
	void	DeleteSection();
	void	Help();
	void	Load();
	void	NewSet();
	void	Paste();
	void	Save();
	void	SelectFont();
	void	Test();

public slots:
	void	SaveAs();

public:
	static EvEdit	*MainWindow()			{ return(main_window);	}

	EvEdit(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~EvEdit();
	
	const QFont&	GetFont()				{ return(ui.centralWidget->font());		}

	const std::string&	Home()			{ return(home);			}
	const std::string&	Version()		{ return(version);		}

	int	GetHight()							{ return(height());		}
	int	GetWidth()							{ return(width());		}

	bool	Warning(const std::string& text);

	void	AddScript(const std::string& display_line);

	void	SetChanged()						{ changed = true;								}
	void	ClearComment()						{ ui.comment_edit->clear();				}
	void	ClearEventDisplay();
	void	ClearFileCategoryDisplay()		{ ui.category_list->clear();				}
	void	ClearFileEventDisplay()			{ ui.events_list->clear();					}
	void	ClearFileSectionDisplay()		{ ui.section_list->clear();				}
	void	ClearListing()						{ ui.listing_edit->clear();				}
	void	ClearScriptDisplay();
	void	ClearScriptListDisplay()		{ ui.script_list->clear();					}
	void	DeselectFileCategoryDisplay()	{ ui.category_list->setCurrentRow(-1);	}
	void	DisableButton(int which)		{ SetButtonEnableStatus(which,false);	}
	void	DisplayCategoryList(const std::list<std::string>& cat_names);
	void	DisplayEvent(const std::string *ev_list);
	void	DisplayEventList(const std::list<std::string>& ev_list);
	void	DisplayScript(const std::string *scr_list,
					const std::string *combo_0 = 0,const std::string *combo_1 = 0);
	void	DisplayScriptList(const std::list<std::string>& scr_list);
	void	DisplaySectionList(const std::list<std::string>& section_names);
	void	EnableButton(int which)			{ SetButtonEnableStatus(which, true);	}
	void	Error(const std::string& text);
	void	List(std::ostringstream& buffer,const std::string colour = "black");
	void	Message(const std::string& text);
	void	SelectCategoryList(const std::string& label);
	void	SelectSectionList(const std::string& label);
	void	SetButtonEnableStatus(int which,bool new_status); 
	void	SetEventNumber(int num)			{ ui.number_spin->setValue(num);			}
	void	Switch2EventDisplay()			{ ui.notebook->setCurrentIndex(0);		}
	void	Switch2FileDisplay()				{ ui.notebook->setCurrentIndex(1);		}
	void	UpdateScriptDisplay(const std::string *scr_list);
	void	WriteStatusBar(const std::string& text);
	void	WriteTemp2StatusBar(const std::string& text);
};

#endif // EVEDIT_H
