/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_insurance.h"

#include "evedit.h"

CheckInsurance::CheckInsurance(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"pass",pass);
		GetAttribute(attribs,"fail",fail);
	}
}

CheckInsurance::CheckInsurance(const std::string *ev_params,const std::string *scr_params)
{
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

CheckInsurance::~CheckInsurance()
{

}


void	CheckInsurance::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checkinsurance";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Pass";
	display[Script::EDIT0] = pass;
	display[Script::LABEL1] = "Fail";
	display[Script::EDIT1] = fail;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckInsurance::Save(std::ofstream& file)
{
	file << "         <checkinsurance";
	if(pass != "")
		file << " pass='" << pass << "'";
	if(fail != "")
		file << " fail='" << fail << "'";
	file << "/>\n";
}

void	CheckInsurance::Update(const std::string *ev_params,const std::string *scr_params)
{
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

bool	CheckInsurance::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == ""))
	{
		EvEdit::MainWindow()->Message("I need at least a pass or a\nfail event for this script!");
		return(false);
	}
	return(true);
}

void	CheckInsurance::Write(std::ostringstream& buffer)
{
	buffer << "checkinsurance script";
	if(pass != "")
		buffer << ":  pass " << pass;
	if(fail != "")
		buffer << ":  fail " << fail;
}

