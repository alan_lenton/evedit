/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "match.h"

#include "evedit.h"

Match::Match(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"id-name",id_name);
		GetAttribute(attribs,"phrase",phrase);
		GetAttribute(attribs,"lo",low);
		GetAttribute(attribs,"hi",high);
		GetAttribute(attribs,"event",ev_num);
	}
}

Match::Match(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	phrase = scr_params[EDIT1];
	low = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	high = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
	ev_num = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT5]);
}

Match::~Match()
{

}


void	Match::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "match";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "ID or name";
	display[Script::EDIT0] = id_name;
	display[Script::LABEL1] = "Phrase";
	display[Script::EDIT1] = phrase;
	display[Script::LABEL3] = "Low";
	display[Script::EDIT3] = low;
	display[Script::LABEL4] = "High";
	display[Script::EDIT4] = high;
	display[Script::LABEL5] = "Event";
	display[Script::EDIT5] = ev_num;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	Match::Save(std::ofstream& file)
{
	file << "         <match";
	file << " id-name='"	<< id_name	<< "'";
	file << " phrase='"	<< phrase	<< "'";
	if(low != "")		file << " lo='"		<< low		<< "'";
	if(high != "")		file << " hi='"		<< high		<< "'";
	if(ev_num != "")	file << " event='"	<< ev_num	<< "'";
	file <<"/>\n";
}

void	Match::Update(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	phrase = scr_params[EDIT1];
	low = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	high = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
	ev_num = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT5]);
}

bool	Match::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("I need an ID or name so I know\nwho the player is talking to!\n");
		return(false);
	}
	if(scr_params[EDIT1] == "")
	{
		EvEdit::MainWindow()->Message("I need a phrase to match!\n");
		return(false);
	}
	if((scr_params[EDIT3] == "") && (scr_params[EDIT4] != ""))
	{
		EvEdit::MainWindow()->Message("You can't have a high\nmessage without a low one!");
		return(false);
	}	
	if((scr_params[EDIT3] == "") && (scr_params[EDIT4] == "") && (scr_params[EDIT5] == ""))
	{
		EvEdit::MainWindow()->Message("You haven't told me what to do if I get a match!");
		return(false);
	}
	if((scr_params[EDIT3] != "") && (scr_params[EDIT5] != ""))
	{
		EvEdit::MainWindow()->Message("I need an event OR a message, not both!");
		return(false);
	}

	return(true);
}

void	Match::Write(std::ostringstream& buffer)
{
	buffer << "match script";

	buffer << ": id/name "	<< id_name;
	buffer << ": phrase "	<< phrase;
	if(low != "")		buffer << ": low "	<< low;
	if(high != "")		buffer << ": high "	<< high;
	if(ev_num != "")	buffer << ": event " << ev_num;
}

