/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "evfile.h"

#include <list>
#include <fstream>
#include <sstream>

#include <cctype>

#include "category.h"
#include "evedit.h"
#include "event.h"
#include "script.h"
#include "script_factory.h"
#include "section.h"


EvFile::~EvFile()
{
	for(CatIndex::iterator iter = index.begin();iter != index.end();++iter)
		delete iter->second;
	index.clear();
}


void	EvFile::AddCategory(Category *category)
{
	index[category->Name()] = category;
	changed = true;
}

void	EvFile::AddEvent(Event *event)
{
	current->AddEvent(event);
	changed = true;
}

void	EvFile::AddScript(const std::string *ev_params,const std::string *scr_params)
{
	if(!ValidateEvent(ev_params) || !ScriptFactory::GetFactory()->ValidateScriptParams(scr_params))
		return;

	if(IsCurrentEvent(ev_params))
	{
		current->AddScript2Current(ev_params,scr_params);
		changed = true;
		return;
	}
	EvEdit::MainWindow()->Message("You need to set up an event before\nyou can add a script to it!");
}

bool	EvFile::CanClose()
{
	if(changed)
		return(EvEdit::MainWindow()->Warning("You haven't saved your work to disk.\nDo you still want to exit?"));
	else
		return(true);
}

void	EvFile::CategorySelected(std::string& cat_name)
{
	current = Find(cat_name);
	if(current == 0)
		EvEdit::MainWindow()->Message("Unable to find that category\nPlease report this problem to ibgames");
	else
		current->DisplaySectionList();
}

void	EvFile::Clear()
{
	for(CatIndex::iterator iter = index.begin();iter != index.end();++iter)
		delete iter->second;
	index.clear();
	changed = false;
}

void	EvFile::ClearFileName()
{ 
	file_name = "";
	EvEdit::MainWindow()->WriteStatusBar(file_name);
}

void	EvFile::DeleteCurrentCategory()
{
	if(current == 0)
	{
		EvEdit::MainWindow()->Error("You haven't selected a category to delete!");
		return;
	}

	std::ostringstream	buffer;
	buffer << "Delete category:\n\n'" << current->Name();
	buffer << "' and all its sections and events.\n\nAre you sure?";
	if(!EvEdit::MainWindow()->Warning(buffer.str()))
		return;
	
	CatIndex::iterator iter = index.find(current->Name());
	if(iter == index.end())
	{
		EvEdit::MainWindow()->Message("I can't seem to find that section\nPlease report this problem to feedback");
		return;
	}
	else
	{
		Category	*category = iter->second;
		index.erase(iter);
		delete category;
		current = 0;
		changed = true;
		DisplayCategoryList();
		EvEdit::MainWindow()->ClearFileEventDisplay();
		EvEdit::MainWindow()->ClearFileSectionDisplay();
		EvEdit::MainWindow()->ClearEventDisplay();
		EvEdit::MainWindow()->ClearListing();
		EvEdit::MainWindow()->ClearScriptDisplay();
		EvEdit::MainWindow()->ClearScriptListDisplay();
		return;
	}
}

void	EvFile::DeleteCurrentEvent()
{
	if(current == 0)
		EvEdit::MainWindow()->Error("You haven't selected an event to delete!");
	else
	{
		if(current->DeleteCurrentEvent())
			changed = true;
	}
}

void	EvFile::DeleteCurrentSection()
{
	if(current == 0)
		EvEdit::MainWindow()->Error("You haven't selected an event to delete!");
	else
	{
		if(current->DeleteCurrentSection())
			changed = true;
	}
}

void	EvFile::DeleteScript(int which)
{
	if(current != 0)
	{
		current->DeleteScript(which);
		changed = true;
	}
}

void	EvFile::DisplayCategoryList()
{
	std::list<std::string>	cat_list;
	for(CatIndex::iterator iter = index.begin();iter != index.end();++iter)
		cat_list.push_back(iter->second->Name());
	EvEdit::MainWindow()->DisplayCategoryList(cat_list);
}


bool	EvFile::EventExists(const std::string *ev_params)
{
	Category *category = Find(ev_params[Script::CATEGORY]);
	if(category == 0)
		return(false);
	else
		return(category->EventExists(ev_params));
}

void	EvFile::EventSelected(std::string& event_number)
{
	if(current == 0)
		EvEdit::MainWindow()->Message("Unable to find the category for this event\nPlease report this problem to ibgames");
	else
		current->DisplayScriptList(event_number);
}

Category	*EvFile::Find(const std::string& cat_name)
{
	CatIndex::iterator	iter = index.find(cat_name);
	if(iter != index.end())
		return(iter->second);
	else
		return(0);
}

void	EvFile::GetComboUpdateInfo(int which_changed,const std::string text,const std::string script_name,std::string *scr_params)
{
	ScriptFactory::GetFactory()->GetComboUpdateInfo(which_changed,text,script_name,scr_params);
}

bool	EvFile::IsCurrentEvent(const std::string *ev_params)
{
	if(current == 0)
		return(false);

	Category	*category = Find(ev_params[Script::CATEGORY]);
	if(category != current)
		return(false);
	else
		return(current->IsCurrentEvent(ev_params));
}

void	EvFile::List()
{
	EvEdit::MainWindow()->ClearListing();
	for(CatIndex::iterator iter = index.begin();iter != index.end();++iter)
		iter->second->List();
}

void	EvFile::MoveScript(int which,int how)
{
	if(current != 0)
	{
		current->MoveScript(which,how);
		changed = true;
	}
}

void	EvFile::NewEvent(std::string *ev_params)
{
	if(!ValidateEvent(ev_params))
		return;

	int next = NextFreeEvNumber(ev_params);
	std::ostringstream	buffer;
	buffer << next;
	ev_params[Script::NUMBER] = buffer.str();

	Category *category = Find(ev_params[Script::CATEGORY]);

	if(category == 0)		// need to set up a new /category
	{
		EvEdit::MainWindow()->ClearFileSectionDisplay();
		EvEdit::MainWindow()->ClearFileEventDisplay();
		EvEdit::MainWindow()->ClearScriptDisplay();
		EvEdit::MainWindow()->ClearScriptListDisplay();
		category = new Category(ev_params[Script::CATEGORY]);
		AddCategory(category);
		current = category;
		DisplayCategoryList();
		EvEdit::MainWindow()->SelectCategoryList(ev_params[Script::CATEGORY]);
	}
	category->NewEvent(ev_params);
	EvEdit::MainWindow()->ClearComment();
	EvEdit::MainWindow()->SetEventNumber(next);
	changed = true;
	EvEdit::MainWindow()->WriteTemp2StatusBar("Event created...");
}

bool	EvFile::NewScript(const std::string& script_name)
{
	Script	*script = ScriptFactory::GetFactory()->CreateScript(script_name);
	if(script == 0)
	{
		EvEdit::MainWindow()->Error("Unable to create that script!\nPlease report the problem to ibgames");
		return(false);
	}
	script->Display();
	EvEdit::MainWindow()->EnableButton(EvEdit::ADD_SCRIPT_BTN);
	EvEdit::MainWindow()->EnableButton(EvEdit::CLEAR_SCRIPT_BTN);
	EvEdit::MainWindow()->Switch2EventDisplay();
	delete script;	// the script is just used to set up the display
	return(true);
}

void	EvFile::NextEvent(std::string *ev_params)
{
	int next = NextFreeEvNumber();
	std::ostringstream	buffer;
	buffer << next;
	ev_params[Script::NUMBER] = buffer.str();
	ev_params[Script::COMMENT] = "";
	EvEdit::MainWindow()->SetEventNumber(next);
	EvEdit::MainWindow()->ClearComment();
	NewEvent(ev_params);
}

int	EvFile::NextFreeEvNumber()
{
	if(current != 0)
		return(current->NextFreeEvNumber());
	else
		return(1);
}

int	EvFile::NextFreeEvNumber(std::string *ev_params)
{
	Category *category = Find(ev_params[Script::CATEGORY]);

	if(category != 0)
		return(category->NextFreeEvNumber(ev_params));
	else
		return(1);
}

int	EvFile::NumScriptsInCurEvent()
{
	if(current != 0)
		return(current->NumScriptsInCurEvent());
	else
		return(0);
}

void	EvFile::Save()
{
	if(file_name == "")
	{
		EvEdit::MainWindow()->SaveAs();
		return;
	}

	std::ofstream	file(file_name.c_str(),std::ios::out | std::ios::trunc);
	if(!file)
	{
		EvEdit::MainWindow()->Error("Unable to open file for writing file!");
		return;
	}
	
	file << "<?xml version=\"1.0\"?>\n\n<event-list version='" << version++;
	file << "' editor='" << EvEdit::MainWindow()->Version() << "'>\n";
	for(CatIndex::iterator iter=index.begin();iter != index.end();iter++)
		iter->second->Save(file);
	file << "</event-list>\n\n";

	changed = false;
	EvEdit::MainWindow()->Message("File saved to disk");
}

bool	EvFile::ScriptSelected(int which)
{
	if(current == 0)
	{
		EvEdit::MainWindow()->Message("Unable to find the category for this script\nPlease report this problem to ibgames");
		return(false);
	}
	current->DisplayScript(which);
	EvEdit::MainWindow()->EnableButton(EvEdit::ADD_SCRIPT_BTN);
	EvEdit::MainWindow()->EnableButton(EvEdit::UPDATE_SCRIPT_BTN);
	EvEdit::MainWindow()->EnableButton(EvEdit::CLEAR_SCRIPT_BTN);
	EvEdit::MainWindow()->Switch2EventDisplay();
	return(true);
}

void	EvFile::SectionSelected(std::string& section_name)
{
	if(current == 0)
		EvEdit::MainWindow()->Message("Unable to find the category for this section\nPlease report this problem to ibgames");
	else
		current->DisplayEventList(section_name);
}

void	EvFile::UpdateComment(const std::string& text)
{
	if(text.length() > 120)
	{
		EvEdit::MainWindow()->Error("Comments should be less that 120 characters!");
		return;
	}
	
	if(current != 0)
	{
		current->UpdateComment(text);
		changed = true;
	}
}

void	EvFile::UpdateScript(const std::string *ev_params,const std::string *scr_params,int which)
{
	if(!ValidateEvent(ev_params) || !ScriptFactory::GetFactory()->ValidateScriptParams(scr_params))
		return;

	if(IsCurrentEvent(ev_params))
	{
		current->UpdateScript(ev_params,scr_params,which);
		changed = true;
	}
}

bool	EvFile::ValidateEvent(const std::string *ev_params)
{
	if(std::isdigit(*(ev_params[Script::CATEGORY].c_str())) || std::isdigit(*(ev_params[Script::SECTION].c_str())))
	{
		EvEdit::MainWindow()->Error("Category and section names may not start with a number!\n");
		return(false);
	}
	if(ev_params[Script::CATEGORY] == "")
	{
		EvEdit::MainWindow()->Error("You must specify a category name!\n");
		return(false);
	}
	if(ev_params[Script::SECTION] == "")
	{
		EvEdit::MainWindow()->Error("You must specify a section name!\n");
		return(false);
	}
	if(ev_params[Script::NUMBER] == "")
	{
		EvEdit::MainWindow()->Error("You must specify an event number!\n");
		return(false);
	}
	
	return(true);
}


/*-------------------------------------------------------
	All the functions and static variables relating solely
	to loading and parsing the file are located below - AL
-------------------------------------------------------*/

const std::string	EvFile::el_names[] = 
	{ "event-list", "category", "section", "event", "comment",	""	};
	
const int	EvFile::NOT_FOUND = 99;


bool	EvFile::characters(const QString& text)
{
	char_data += text;
	return(true);
}

bool	EvFile::endElement(const QString& namespaceURI,const QString& localName,const QString& qName)
{
	switch(FindElement(qName.toStdString()))
	{
		case	0:	EndParsing();
		case	1:	if(current != 0)
						AddCategory(current);
					current = 0;
					break;
		case	3:	if(current != 0)
						AddEvent(current_event);
					current_event = 0; 
					break;
		case	4: if(current_event != 0)
						current_event->AddComment(char_data.toStdString());
					break;
		default:	EndScript();	break;
	}
	return(true);
}

void	EvFile::EndParsing()
{
	current = 0;
	current_event = 0;
	current_script = 0;
	changed = false;
	DisplayCategoryList();
}

void	EvFile::EndScript()
{
	if(current_script == 0)
		return;
	current_script->AddText(char_data.toStdString());
	if(current_event != 0)
		current_event->AddScript(current_script);
	current_script = 0;
}

int	EvFile::FindElement(const std::string& element)
{
	for(int which = 0;el_names[which] != "";++which)
	{
		if(element == el_names[which])
			return(which);
	}
	return(NOT_FOUND);
}

void	EvFile::Load()
{
	if(CanClose())
	{
		Clear();
		EvEdit::MainWindow()->ClearEventDisplay();
		EvEdit::MainWindow()->ClearFileEventDisplay();
		EvEdit::MainWindow()->ClearFileSectionDisplay();
		EvEdit::MainWindow()->ClearListing();
		EvEdit::MainWindow()->ClearScriptDisplay();
		EvEdit::MainWindow()->ClearScriptListDisplay();
		EvEdit::MainWindow()->ClearFileCategoryDisplay();

		QFile	file(QString::fromStdString(file_name));
		if(file.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			QXmlInputSource	source(&file);
			QXmlSimpleReader	reader;
			reader.setContentHandler(this);
			reader.parse(source);
			file.close();
			EvEdit::MainWindow()->Switch2FileDisplay();
			EvEdit::MainWindow()->WriteStatusBar(file_name);
			changed = false;
		}
		else
			EvEdit::MainWindow()->Error("Unable to open file for reading!");
	}
}

void	EvFile::StartCategory(const QXmlAttributes& attribs)
{
	std::string	cat_name(attribs.value(QString::fromLatin1("name")).toStdString());
	if(cat_name != "")
		current = new Category(cat_name);
	else
		EvEdit::MainWindow()->Error("Unable to find category name in .xev file!");
}

void	EvFile::StartComment(const QXmlAttributes& attribs)
{
	char_data = "";
}

bool	EvFile::startElement(const QString& namespaceURI,const QString& localName,
												const QString& qName, const QXmlAttributes& atts)
{
	switch(FindElement(qName.toStdString()))
	{
		case  0:	StartEventList(atts);	break;
		case	1:	StartCategory(atts);		break;
		case	2:	StartSection(atts);		break;
		case	3: StartEvent(atts);			break;
		case	4:	StartComment(atts);		break;
		default:	StartScript(qName.toStdString(),atts);	break;
	}

	return(true);
}

void	EvFile::StartEvent(const QXmlAttributes& attribs)
{
	int	number = attribs.value(QString::fromLatin1("num")).toInt();
	if(number != 0)
		current_event = new Event(number);
	else
		EvEdit::MainWindow()->Error("Unable to find event number in .xev file!");
}
	
void	EvFile::StartEventList(const QXmlAttributes& attribs)
{
	version = attribs.value(QString::fromLatin1("version")).toInt();
	if(version == 0)
		version = 1;
	else
		++version;
}

void	EvFile::StartScript(const std::string& script_name,const QXmlAttributes& attribs)
{
	current_script = ScriptFactory::GetFactory()->CreateScript(script_name,&attribs);
	if(current_script != 0)
		char_data = "";
}

void	EvFile::StartSection(const QXmlAttributes& attribs)
{
	std::string	sect_name(attribs.value(QString::fromLatin1("name")).toStdString());
	if(sect_name != "")
	{
		Section	*section = new Section(sect_name);
		if(current != 0)
			current->AddSection(section);
		else	/******** really need to abort here - unrecoverable error ********/
			EvEdit::MainWindow()->Error("Found a section that's not part of a category!\n");
	}
	else
		EvEdit::MainWindow()->Error("Unable to find section name in .xev file!");
}

