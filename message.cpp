/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "message.h"
#include "evedit.h"


const int	Message::MAX_COMBO_0 = 6;


Message::Message(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"who",who);
		GetAttribute(attribs,"lo",low);
		GetAttribute(attribs,"hi",high);
	}
}

Message::Message(const std::string *ev_params,const std::string *scr_params)			
{
	who = scr_params[COMBO0];
	low = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	high = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	text = scr_params[TEXTEDIT];
}

Message::~Message()
{

}


void	Message::AddText(const std::string& new_text)			
{ 
	text = new_text;	
}

void	Message::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "message";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Low";
	display[Script::EDIT0] = low;
	display[Script::LABEL1] = "High";
	display[Script::EDIT1] = high;
		
	// Change MAX_COMBO_0 if you add to this!
	std::ostringstream buffer;
	buffer << (FindComboIndex(who));
	std::string	combo_0[MAX_COMBO_0];
	combo_0[0] = "To whom";
	combo_0[1] = buffer.str();
	combo_0[2] = "individual";
	combo_0[3] = "room";
	combo_0[4] = "room_ex";
	combo_0[5] = "";

	display[Script::TEXTLAB] = "Message text";
	display[Script::TEXTEDIT] = text;

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

int	Message::FindComboIndex(const std::string& which) const
{
	static const std::string	combo_names[] = { "individual", "room", "room_ex", ""	};

	for(int count = 0;combo_names[count] != "";++count)
	{
		if(combo_names[count] == which)
			return(count);
	}
	return(0);
}

void	Message::Save(std::ofstream& file)
{
	int	type = FindTextType(low,high,text);
	file << "         <message type='" << mssg_types[type] << "' where='com'";
	file << " who='" << who << "'";
	if(low != "")
		file << " lo='" << low << "'";
	if(high != "")
		file << " hi='" << high << "'";

	if(text == "")
		file << "/>\n";
	else
	{
		std::string temp(text);
		file << ">" << EscapeXML(CleanText(temp)) << "</message>\n";
	}
}

void	Message::Update(const std::string *ev_params,const std::string *scr_params)
{
	who = scr_params[COMBO0];
	low = scr_params[EDIT0];
	high = scr_params[EDIT1];
	text = scr_params[TEXTEDIT];
}

bool	Message::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == "") && (scr_params[TEXTEDIT] == ""))
	{
		EvEdit::MainWindow()->Message("You haven't given me a message to send!");
		return(false);
	}
	if(((scr_params[EDIT0] != "") || (scr_params[EDIT1] != "")) && (scr_params[TEXTEDIT] != ""))
	{
		EvEdit::MainWindow()->Message("You've given me two different messages\n- please remove one of them!");
		return(false);
	}
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] != ""))
	{
		EvEdit::MainWindow()->Message("You have given me a 'high' message number,\nbut no 'low' message number!\n");
		return(false);
	}
	if(scr_params[TEXTEDIT].length() > 159)
	{
		EvEdit::MainWindow()->Message("Your message is too long (over 159 characters)\nplease move it to the message file!\n");
		return(false);
	}
	
	return(true);
}

void	Message::Write(std::ostringstream& buffer)
{
	int	type = FindTextType(low,high,text);
	buffer << "message script:  type " << mssg_types[type];
	buffer << ":  to whom " << who;
	if(low != "")
		buffer << ":  low " << low;
	if(high != "")
		buffer << " to high " << high;
	if(text != "")
		buffer << ": text '" << CleanText(text) << "'";
}

