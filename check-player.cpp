/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_player.h"
#include "evedit.h"

CheckPlayer::CheckPlayer(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"sta",stamina);
		GetAttribute(attribs,"str",strength);
		GetAttribute(attribs,"dex",dexterity);
		GetAttribute(attribs,"int",intell);

		GetAttribute(attribs,"pass",pass);
		GetAttribute(attribs,"fail",fail);
	}
}

CheckPlayer::CheckPlayer(const std::string *ev_params,const std::string *scr_params)
{
	stamina = scr_params[EDIT0];
	strength = scr_params[EDIT1];
	dexterity = scr_params[EDIT2];
	intell = scr_params[EDIT3];
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT5]);
}

CheckPlayer::~CheckPlayer()
{

}


void	CheckPlayer::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checkplayer";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Stamina";
	display[Script::EDIT0] = stamina;
	display[Script::LABEL1] = "Strength";
	display[Script::EDIT1] = strength;
	display[Script::LABEL2] = "Dexterity";
	display[Script::EDIT2] = dexterity;
	display[Script::LABEL3] = "Intelligence";
	display[Script::EDIT3] = intell;

	display[Script::LABEL4] = "Pass";
	display[Script::EDIT4] = pass;
	display[Script::LABEL5] = "Fail";
	display[Script::EDIT5] = fail;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckPlayer::Save(std::ofstream& file)
{
	file << "         <checkplayer";

	if(stamina != "")		file << " sta='"  << stamina   << "'";
	if(strength != "")	file << " str='"  << strength  << "'";
	if(dexterity != "")	file << " dex='"  << dexterity << "'";
	if(intell != "")		file << " int='"  << intell    << "'";
	
	if(pass != "")			file << " pass='" << pass      << "'";
	if(fail != "")			file << " fail='" << fail      << "'";

	file <<"/>\n";
}

void	CheckPlayer::Update(const std::string *ev_params,const std::string *scr_params)
{
	stamina = scr_params[EDIT0];
	strength = scr_params[EDIT1];
	dexterity = scr_params[EDIT2];
	intell = scr_params[EDIT3];

	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT5]);
}

bool	CheckPlayer::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == "") && (scr_params[EDIT2] == "") 
																						&& (scr_params[EDIT3] == ""))
	{
		EvEdit::MainWindow()->Message("You haven't given me any\nplayer stats numbers to check against!");
		return(false);
	}

	if((scr_params[EDIT4] == "") && (scr_params[EDIT5] == ""))
	{
		EvEdit::MainWindow()->Message("I need at least a pass or a\nfail event for this script!");
		return(false);
	}
	return(true);
}

void	CheckPlayer::Write(std::ostringstream& buffer)
{
	buffer << "checkplayer script";

	if(stamina != "")		buffer << ": stamina "			<< stamina;
	if(strength != "")	buffer << ": strength "			<< strength;
	if(dexterity != "")	buffer << ": dexterity "		<< dexterity;
	if(intell != "")		buffer << ": intelligence "	<< intell;
	
	if(pass != "")			buffer << ": pass " << pass;
	if(fail != "")			buffer << ": fail " << fail;
}


