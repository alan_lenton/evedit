/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_flag.h"
#include "evedit.h"

const int   CheckFlag::MAX_COMBO_0 = 11;

CheckFlag::CheckFlag(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"flag",flag);
		GetAttribute(attribs,"pass",pass);
		GetAttribute(attribs,"fail",fail);
	}
}

CheckFlag::CheckFlag(const std::string *ev_params,const std::string *scr_params)
{
	flag = scr_params[COMBO0];
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

CheckFlag::~CheckFlag()
{

}


void CheckFlag::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];

	display[SCRIPT] = "checkflag";
	display[LEVEL] = staff_only;

	display[Script::LABEL0] = "Pass";
	display[Script::EDIT0] = pass;
	display[Script::LABEL1] = "Fail";
	display[Script::EDIT1] = fail;

	std::string combo_0[MAX_COMBO_0];
	combo_0[0] = "Flag name";
	combo_0[1] = "0";
	combo_0[2] = "ship-permit";
	combo_0[3] = "trade-permit";
	combo_0[4] = "id-card";
	combo_0[5] = "medal";
	combo_0[6] = "insured";
	combo_0[7] = "staff";
	combo_0[8] = "nav-comp";
	combo_0[9] = "customs-cert";
	combo_0[10] = "";

	if(flag != "")
	{
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			if(flag == combo_0[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_0[1] = buffer.str();
				break;
			}
		}
	}
	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	CheckFlag::Save(std::ofstream& file)
{
	file << "         <checkflag flag='" << flag << "'";
	if(pass != "")
		file << " pass='" << pass << "'";
	if(fail != "")
		file << " fail='" << fail << "'";
	file << "/>\n";
}

void CheckFlag::Update(const std::string *ev_params, const std::string *scr_params)
{
	flag = scr_params[COMBO0];
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT0]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
}

bool	CheckFlag::Validate(const std::string *scr_params)
{
	if((scr_params[EDIT0] == "") && (scr_params[EDIT1] == ""))
	{
		EvEdit::MainWindow()->Message("I need at least a pass or a\nfail event for this script!");
		return(false);
	}
	return(true);
}

void CheckFlag::Write(std::ostringstream& buffer)
{
	buffer << "checkflag script:  flag " << flag;
	if(pass != "")
		buffer << ":  pass " << pass;
	if(fail != "")
		buffer << ":  fail " << fail;
}



