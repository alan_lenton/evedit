/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "move.h"
#include "evedit.h"

Move::Move(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"what",what);
		GetAttribute(attribs,"silent",silent);
		(silent == "true") ? silent = "y" : silent = "n";
		GetAttribute(attribs,"id-name",id_name);
		GetAttribute(attribs,"loc",loc_no);
	}
}

Move::Move(const std::string *ev_params,const std::string *scr_params)
{
	what = scr_params[COMBO0];
	loc_no = scr_params[EDIT0];
	id_name = scr_params[EDIT1];
	silent = scr_params[EDIT2];
}

Move::~Move()
{

}


void	Move::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "move";
	display[Script::LEVEL] = planet_owners;

	display[Script::LABEL0] = "To location";
	display[Script::EDIT0] = loc_no;
	
	// Change MAX_COMBO_0 if you add to this!
	std::ostringstream buffer;
	buffer << ((what == "object") ? "1" : "0");
	std::string	combo_0[MAX_COMBO_0];
	combo_0[0] = "What to move";
	combo_0[1] = buffer.str();
	combo_0[2] = "player";
	combo_0[3] = "object";
	combo_0[4] = "";

	if(buffer.str() == "1")
	{
		display[LABEL1] = "ID or name";
		display[EDIT1] = id_name;
		display[LABEL2] = "Silent (y/n)?";
		display[EDIT2] = silent;
	}

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	Move::GetComboUpdateInfo(int which_changed,const std::string text,std::string *scr_params)
{
	if(text == "object")
	{
		scr_params[LABEL1] = "ID or name";
		scr_params[LABEL2] = "Silent (y/n)?";
	}

	if(text == "player")
	{
		scr_params[LABEL1] = "~disable";
		scr_params[LABEL2] = "~disable";
	}
}

void	Move::Save(std::ofstream& file)
{
	file << "         <move what='" << what << "'";
	if(what == "player")	file << " who='individual'";
	if(silent[0] == 'y')		file << " silent='true'";
	if(id_name != "")		file << " id-name='" << id_name << "'";
	file << " loc='" << loc_no << "'/>\n";
}

void	Move::Update(const std::string *ev_params,const std::string *scr_params)
{
	what = scr_params[COMBO0];
	loc_no = scr_params[EDIT0];
	id_name = scr_params[EDIT1];
	silent = scr_params[EDIT2];
}

bool	Move::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a location to move to!");
		return(false);
	}
	if((scr_params[COMBO0] == "object") && (scr_params[EDIT1] == ""))
	{
		EvEdit::MainWindow()->Message("You haven't told me which object to move!");
		return(false);
	}

	return(true);
}

void	Move::Write(std::ostringstream& buffer)
{
	buffer << "move script:  what " << what;
	if(silent[0] == 'y')		buffer << ":  silent yes";
	if(id_name != "")		buffer << ":  id/name " << id_name;
	buffer << ":  to location " << loc_no;
}

