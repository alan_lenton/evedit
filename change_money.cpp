/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "change_money.h"

#include "evedit.h"

ChangeMoney::ChangeMoney(const QXmlAttributes *attribs)
{
	if(attribs != 0)
		GetAttribute(attribs,"amount",value);
}

ChangeMoney::ChangeMoney(const std::string *ev_params,const std::string *scr_params)
{
	value = scr_params[EDIT0];
}

ChangeMoney::~ChangeMoney()
{

}

void	ChangeMoney::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "changemoney";
	display[Script::LEVEL] = planet_owners;
	display[Script::LABEL0] = "Amount";
	display[Script::EDIT0] = value;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	ChangeMoney::Save(std::ofstream& file)
{
	file << "         <changemoney amount='" << value << "'/>\n";
}

void	ChangeMoney::Update(const std::string *ev_params,const std::string *scr_params)
{
	value = scr_params[EDIT0];
}

bool	ChangeMoney::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't told me what\namount to change to!\n");
		return(false);
	}
	return(true);
}

void	ChangeMoney::Write(std::ostringstream& buffer)
{
	buffer << "changemoney script:  amount " << value;
}

