/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "flip_flag.h"
#include "evedit.h"

const int   FlipFlag::MAX_COMBO_0 = 11;

FlipFlag::FlipFlag(const QXmlAttributes *attribs)
{
	if(attribs != 0)
		GetAttribute(attribs,"flag",flag);
}

FlipFlag::FlipFlag(const std::string *ev_params,const std::string *scr_params)
{
	flag = scr_params[COMBO0];
}

FlipFlag::~FlipFlag()
{

}


void FlipFlag::Display()
{
	std::string	display[MAX_SCRIPT_DISPLAY];

	display[SCRIPT] = "flipflag";
	display[LEVEL] = staff_only;

	std::string combo_0[MAX_COMBO_0];
	combo_0[0] = "Flag name";
	combo_0[1] = "0";
	combo_0[2] = "ship-permit";
	combo_0[3] = "trade-permit";
	combo_0[4] = "id-card";
	combo_0[5] = "medal";
	combo_0[6] = "insured";
	combo_0[7] = "staff";
	combo_0[8] = "nav-comp";
	combo_0[9] = "customs-cert";
	combo_0[10] = "";

	if(flag != "")
	{
		for(int count = 2; count < MAX_COMBO_0 - 1;++count)
		{
			if(flag == combo_0[count])
			{
				std::ostringstream	buffer;
				buffer << (count - 2);
				combo_0[1] = buffer.str();
				break;
			}
		}
	}
	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	FlipFlag::Save(std::ofstream& file)
{
	file << "         <flipflag flag='" << flag << "'/>\n";
}

void FlipFlag::Update(const std::string *ev_params, const std::string *scr_params)
{
	flag = scr_params[COMBO0];
}

void FlipFlag::Write(std::ostringstream& buffer)
{
	buffer << "flipflag script:  flag " << flag;
}


