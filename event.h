/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef EVENT_H
#define EVENT_H

#include <fstream>
#include <list>
#include <string>

class	Script;

typedef	std::list<Script *>	ScriptList;

class	Event
{
private:
	int			number;
	Script		*current;
	std::string	comment;
	ScriptList	script_list;
	
	/******** May need to impliment these later & make public ********/
	Event(const Event& rhs);
	Event& operator=(const Event& rhs);
	
	Script	*Find(int index);

public:
	Event(int num) : number(num),current(0)		{	} 
	~Event();

	const std::string	Comment()						{ return(comment);				}
	const std::string NumberAndComment();

	int	Number()											{ return(number);					}
	int	NumScripts()									{ return(script_list.size());	}
	
	bool	AddScript(const std::string *ev_params,const std::string *scr_params);

	void	AddComment(const std::string& text);
	void	AddScript(Script *script);
	void	ClearCurrent()									{ current = 0;						}
	void	DeleteScript(int which);
	void	DisplayScript(int which);
	void	DisplayScriptList(const std::string& cat_name,const std::string& section_name);
	void	List();
	void	MoveScript(int which,int how);
	void	Save(std::ofstream& file);
	void	UpdateScript(const std::string *ev_params,const std::string *scr_params,int which);						
};

#endif
