/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_inventory.h"

#include "evedit.h"

CheckInventory::CheckInventory(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"map",map_name);
		GetAttribute(attribs,"id-name",id_name);
		GetAttribute(attribs,"found",found);
		GetAttribute(attribs,"not-found",not_found);
	}
}

CheckInventory::CheckInventory(const std::string *ev_params,const std::string *scr_params)
{
	map_name = scr_params[EDIT0];
	id_name = scr_params[EDIT1];
	found = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	not_found = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

CheckInventory::~CheckInventory()
{

}

void	CheckInventory::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checkinventory";
	display[Script::LEVEL] = planet_owners;

	display[Script::LABEL0] = "Map Name";
	if(map_name == "")
		display[Script::EDIT0] = "home";
	else
		display[Script::EDIT0] = map_name;
	display[Script::LABEL1] = "ID or Name";
	display[Script::EDIT1] = id_name;
	display[Script::LABEL3] = "Found";
	display[Script::EDIT3] = found;
	display[Script::LABEL4] = "Not Found";
	display[Script::EDIT4] = not_found;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckInventory::Save(std::ofstream& file)
{
	file << "         <checkinventory map='" << map_name << "'";
	file << " id-name='" << id_name << "'";
	if(found != "")
		file << " found='" << found << "'";
	if(not_found != "")
		file << " not-found='" << not_found << "'";
	file << "/>\n";
}

void	CheckInventory::Update(const std::string *ev_params,const std::string *scr_params)
{
	map_name = scr_params[EDIT0];
	id_name = scr_params[EDIT1];
	found = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT3]);
	not_found = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT4]);
}

bool	CheckInventory::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't told me what\nmap to check for!\n");
		return(false);
	}
	if(scr_params[EDIT1] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me an\nobject ID or name to check for!\n");
		return(false);
	}
	if((scr_params[EDIT3] == "") && (scr_params[EDIT4] == ""))
	{
		EvEdit::MainWindow()->Message("You need to give me at least\none event to call!\n");
		return(false);
	}
	return(true);
}

void	CheckInventory::Write(std::ostringstream& buffer)
{
	buffer << "checkinventory script:  map " << map_name;
	buffer << ":  id/name " << id_name;
	if(found != "")
		buffer << ":  found " << found;
	if(not_found != "")
		buffer << ":  not-found " << not_found;
}

