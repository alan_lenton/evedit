/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "set_variable.h"
#include "evedit.h"


const int	SetVariable::MAX_COMBO_0 = 5;

SetVariable::SetVariable(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"name",name);
		GetAttribute(attribs,"key",key);
		GetAttribute(attribs,"value",value);
		GetAttribute(attribs,"temporary",type);
		if(type == "")
			type = "permanent";
		else
			type = "temporary";
	}
}

SetVariable::SetVariable(const std::string *ev_params,const std::string *scr_params)
{
	type = scr_params[COMBO0];
	name = scr_params[EDIT0];
	key = scr_params[EDIT1];
	value = scr_params[EDIT2];
}

SetVariable::~SetVariable()
{

}


void	SetVariable::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "setvariable";
	display[Script::LEVEL] = planet_owners;

	display[Script::LABEL0] = "Name";
	display[Script::EDIT0] = name;
	display[Script::LABEL1] = "Key";
	display[Script::EDIT1] = key;
	display[Script::LABEL2] = "Value";
	display[Script::EDIT2] = value;
	
	// Change MAX_COMBO_0 if you add to this!
	std::string	combo_0[MAX_COMBO_0];
	combo_0[0] = "type of event";
	combo_0[1] = "0";
	combo_0[2] = "temporary";
	combo_0[3] = "permanent";
	combo_0[4] = "";

	if(type == "permanent")
		combo_0[1] = "1";

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	SetVariable::Save(std::ofstream& file)
{
	file << "         <setvariable name='" << name << "'";
	file << " key='" << key << "'";
	file << " value='" << value << "'";
	if(type == "temporary")		file << " temporary='true" << "'";
	file << "/>\n";
}

void	SetVariable::Update(const std::string *ev_params,const std::string *scr_params)
{
	type = scr_params[COMBO0];
	name = scr_params[EDIT0];
	key = scr_params[EDIT1];
	value = scr_params[EDIT2];
}

bool	SetVariable::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a name!");
		return(false);
	}

	if(scr_params[EDIT1] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a key!");
		return(false);
	}

	if(scr_params[EDIT2] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a value!");
		return(false);
	}

	return(true);
}

void	SetVariable::Write(std::ostringstream& buffer)
{
	buffer << "setvariable script:  name " << name;
	buffer << ":  key " << key;
	buffer << ":  value " << value;
	if(type == "temporary")
		buffer << ":  temporary true";
}

