/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "check_last_loc.h"

#include "evedit.h"

CheckLastLoc::CheckLastLoc(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"loc",last_loc);
		GetAttribute(attribs,"pass",pass);
		GetAttribute(attribs,"fail",fail);
	}
}

CheckLastLoc::CheckLastLoc(const std::string *ev_params,const std::string *scr_params)
{
	last_loc = scr_params[EDIT0];
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

CheckLastLoc::~CheckLastLoc()
{

}


void	CheckLastLoc::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "checklastloc";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Last location";
	display[Script::EDIT0] = last_loc;
	display[Script::LABEL1] = "Pass";
	display[Script::EDIT1] = pass;
	display[Script::LABEL2] = "Fail";
	display[Script::EDIT2] = fail;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	CheckLastLoc::Save(std::ofstream& file)
{
	file << "         <checklastloc loc='" << last_loc << "'";
	if(pass != "")
		file << " pass='" << pass << "'";
	if(fail != "")
		file << " fail='" << fail << "'";
	file <<"/>\n";
}

void	CheckLastLoc::Update(const std::string *ev_params,const std::string *scr_params)
{
	last_loc = scr_params[EDIT0];
	pass = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	fail = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
}

bool	CheckLastLoc::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a\nlocation number to check against!");
		return(false);
	}
	if((scr_params[EDIT1] == "") && (scr_params[EDIT2] == ""))
	{
		EvEdit::MainWindow()->Message("I need at least a pass or a\nfail event for this script!");
		return(false);
	}
	return(true);
}

void	CheckLastLoc::Write(std::ostringstream& buffer)
{
	buffer << "checklastloc script:  location " << last_loc;
	if(pass != "")
		buffer << ":  pass " << pass;
	if(fail != "")
		buffer << ":  fail " << fail;
}

