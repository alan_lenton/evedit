/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef SECTION_H
#define SECTION_H

#include <map>
#include <string>

class Event;

typedef std::map<int,Event *,std::less<int> >	EventIndex;

class	Section
{
private:
	std::string	name;
	Event			*current;
	EventIndex	index;

	Section(const Section& rhs);
	Section& operator=(const Section& rhs);

	Event	*Find(int event_number);

public:
	Section(const std::string& sec_name) : name(sec_name), current(0)	{	}
	~Section();

	const std::string&	Name()		{ return(name);	}

	int	NextFreeEvNumber();
	int	NumScriptsInCurEvent();

	bool	DeleteCurrentEvent();
	bool	EventExists(const std::string *ev_params);
	bool	IsCurrentEvent(const std::string *ev_params);

	void	AddEvent(Event *event);
	void	AddScript2Current(const std::string *ev_params,const std::string *scr_params);
	void	ClearAllCurrent();
	void	DeleteScript(int which);
	void	DisplayEventList();
	void	DisplayScript(int which);
	void	DisplayScriptList(const std::string& cat_name,const std::string& event_number);
	void	List();
	void	MoveScript(int which,int how);
	void	NewEvent(const std::string *ev_params);
	void	Save(std::ofstream& file);
	void	UpdateComment(const std::string& text);						
	void	UpdateScript(const std::string *ev_params,const std::string *scr_params,int which);
};

#endif

