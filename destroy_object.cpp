/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "destroy_object.h"

#include "evedit.h"


const int	DestroyObject::MAX_COMBO_0 = 6;

DestroyObject::DestroyObject(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"map",map_name);
		GetAttribute(attribs,"id-name",id_name);
		GetAttribute(attribs,"where",where);
	}
}

DestroyObject::DestroyObject(const std::string *ev_params,const std::string *scr_params)
{
	map_name = scr_params[EDIT0];
	id_name = scr_params[EDIT1];
	where = scr_params[COMBO0];
}

DestroyObject::~DestroyObject()
{

}


void	DestroyObject::Display()
{
	static const std::string	combo_names[]	= { "inventory", "room", "both", ""	};

	std::string	display[MAX_SCRIPT_DISPLAY];

	display[SCRIPT] = "destroyobject";
	display[LEVEL] = planet_owners;

	display[LABEL0] = "Map Name";
	if(map_name != "")
		display[EDIT0] = map_name;
	else
		display[EDIT0] = "home";
	display[LABEL1] = "ID or name";
	display[EDIT1] = id_name;

	// Change MAX_COMBO_0 if you add to this!
	std::string	combo_0[MAX_COMBO_0];	// currently 6
	combo_0[0] = "Where";
	combo_0[1] = "0";
	for(int count = 0;(combo_names[count] != "") && (count < (MAX_COMBO_0 - 3));count++)
	{
		if(where == combo_names[count])
		{
			std::ostringstream buffer;
			buffer << count;
			combo_0[1] = buffer.str();
		}
		combo_0[count + 2] = combo_names[count];
	}
	combo_0[5] = "";

	EvEdit::MainWindow()->DisplayScript(display,combo_0);
}

void	DestroyObject::Save(std::ofstream& file)
{
	file << "         <destroyobject where='" << where << "'";
	file << " map='" << map_name << "'";
	file << " id-name='" << id_name << "'/>\n";
}

void	DestroyObject::Update(const std::string *ev_params,const std::string *scr_params)
{
	where = scr_params[COMBO0];
	map_name = scr_params[EDIT0];
	id_name = scr_params[EDIT1];
}

bool	DestroyObject::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a map name!");
		return(false);
	}
	if(scr_params[EDIT1] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me an object ID or name!");
		return(false);
	}

	return(true);
}

void	DestroyObject::Write(std::ostringstream& buffer)
{
	buffer << "destroyobject script:  where " << where;
	buffer << ":  map " << map_name;
	buffer << ":  id/name " << id_name;
}

