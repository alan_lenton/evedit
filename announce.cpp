/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "announce.h"

#include "evedit.h"

Announce::Announce(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"hi",high);
		GetAttribute(attribs,"lo",low);
		GetAttribute(attribs,"loc",loc_num);
	}
}

Announce::Announce(const std::string *ev_params,const std::string *scr_params)
{
	loc_num = scr_params[EDIT0];
	low = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	high = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
	text = scr_params[TEXTEDIT];
}

Announce::~Announce()
{

}


void	Announce::AddText(const std::string& new_text)			
{ 
	text = new_text;	
}

void	Announce::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];
	display[Script::SCRIPT] = "announce";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "Location";
	display[Script::EDIT0] = loc_num;
	display[Script::LABEL1] = "Low";
	display[Script::EDIT1] = low;
	display[Script::LABEL2] = "High";
	display[Script::EDIT2] = high;

	display[Script::TEXTLAB] = "Message text";
	display[Script::TEXTEDIT] = text;
	EvEdit::MainWindow()->DisplayScript(display);
}

void	Announce::Save(std::ofstream& file)
{
	int	text_type = FindTextType(low,high,text);
	file << "         <announce type='" << mssg_types[text_type] << "' loc='" << loc_num;
	switch(text_type)
	{
		case SINGLE:	file << "' lo='" << low << "'/>";										break;
		case MULTI:		file << "' lo='" << low << "' hi='" << high << "'/>";				break;
		case TEXT:		
			std::string temp(text);
			file << "'>" << EscapeXML(CleanText(temp)) << "</announce>";
			break;
	}
	file << "\n";
}

void	Announce::Update(const std::string *ev_params,const std::string *scr_params)
{
	loc_num = scr_params[EDIT0];
	low = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT1]);
	high = MakeFullName(ev_params[CATEGORY],ev_params[SECTION],scr_params[EDIT2]);
	text = scr_params[TEXTEDIT];
}	

bool	Announce::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You must specify a location in\nwhich to make the announcement!\n");
		return(false);
	}

	if((scr_params[EDIT1] == "") && (scr_params[EDIT2] == "") && (scr_params[TEXTEDIT] == ""))
	{
		EvEdit::MainWindow()->Message("You haven't given me a message to announce!\n");
		return(false);
	}
	
	if((scr_params[EDIT1] != "") && (scr_params[TEXTEDIT] != ""))
	{
		EvEdit::MainWindow()->Message("You have given me both a message number and some\nmessage text to announce. One or the other, please!\n");
		return(false);
	}
	
	if((scr_params[EDIT1] == "") && (scr_params[EDIT2] != ""))
	{
		EvEdit::MainWindow()->Message("You have given me a 'high' message number,\nbut no 'low' message number!\n");
		return(false);
	}

	if(scr_params[TEXTEDIT].length() > 160)
	{
		EvEdit::MainWindow()->Message("Your message is too long (over 160 characters)\nplease move it to the message file!\n");
		return(false);
	}
	
	return(true);
}

void	Announce::Write(std::ostringstream& buffer)
{
	int	text_type = FindTextType(low,high,text);
	buffer << "announce script:  location " << loc_num;
	switch(text_type)
	{
		case SINGLE:	buffer << ":  low " << low;									break;
		case MULTI:		buffer << ":  low " << low << " to high " << high;				break;
		case TEXT:		buffer << ":  text '" << CleanText(text) << "'";	break;
	}
}




