/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef CHECKMAP_H
#define CHECKMAP_H

#include	<string>

#include "script.h"

class CheckMap : public Script
{
private:
	std::string	map_name;
	std::string	loc_no;
	std::string	pass;
	std::string	fail;

	CheckMap(const CheckMap& rhs);
	CheckMap& operator=(const CheckMap& rhs);

public:
	static bool	Validate(const std::string *scr_params);

	CheckMap(const QXmlAttributes *attribs = 0);
	CheckMap(const std::string *ev_params,const std::string *scr_params);
	~CheckMap();

	void	Display();
	void	Save(std::ofstream& file);
	void	Update(const std::string *ev_params,const std::string *scr_params);
	void	Write(std::ostringstream& buffer);
};

#endif
