/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2053 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "display_size.h"

#include "evedit.h"


DisplaySize::DisplaySize(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"id-name",id_name);

	}
}

DisplaySize::DisplaySize(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	text = scr_params[TEXTEDIT];
}

DisplaySize::~DisplaySize()
{

}


void	DisplaySize::AddText(const std::string& new_text)			
{ 
	text = new_text;	
}

void	DisplaySize::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "displaysize";
	display[Script::LEVEL] = planet_owners;
	
	display[Script::LABEL0] = "ID or name";
	display[Script::EDIT0] = id_name;
		
	display[Script::TEXTLAB] = "Message text";
	display[Script::TEXTEDIT] = text;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	DisplaySize::Save(std::ofstream& file)
{
	file << "         <displaysize id-name='" << id_name << "'";
	if(text == "")
		file << "/>\n";
	else
	{
		std::string temp(text);
		file << ">" << EscapeXML(CleanText(temp)) << "</displaysize>\n";
	}
}

void	DisplaySize::Update(const std::string *ev_params,const std::string *scr_params)
{
	id_name = scr_params[EDIT0];
	text = scr_params[TEXTEDIT];
}

bool	DisplaySize::Validate(const std::string *scr_params)
{
	if(scr_params[TEXTEDIT] == "")
	{
		EvEdit::MainWindow()->Message("You haven't given me a message to send!");
		return(false);
	}

	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("You've not given me an object to display the size of!");
		return(false);
	}

	if(scr_params[TEXTEDIT].find("%d") == std::string::npos)
	{
		EvEdit::MainWindow()->Message("You've not given me a '%d' to show where the number goes!");
		return(false);
	}

	if(scr_params[TEXTEDIT].length() > 160)
	{
		EvEdit::MainWindow()->Message("Your message is too long (over 160 characters)\nplease move it to the message file!\n");
		return(false);
	}
	
	return(true);
}

void	DisplaySize::Write(std::ostringstream& buffer)
{
	buffer << "displaysize script:  id-name " << id_name;
	buffer << ": text '" << CleanText(text) << "'";
}

