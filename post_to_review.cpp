/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "post_to_review.h"
#include "evedit.h"


PostToReview::PostToReview(const std::string *ev_params,const std::string *scr_params)
{
	text = scr_params[TEXTEDIT];
}

PostToReview::~PostToReview()
{

}


void	PostToReview::AddText(const std::string& new_text)			
{ 
	text = new_text;	
}


void	PostToReview::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "posttoreview";
	display[Script::LEVEL] = staff_only;
	
	display[Script::TEXTLAB] = "Message text";
	display[Script::TEXTEDIT] = text;

	EvEdit::MainWindow()->DisplayScript(display);
}

void	PostToReview::Save(std::ofstream& file)
{
	file << "         <posttoreview>";
	std::string temp(text);
	file << EscapeXML(CleanText(temp)) << "</posttoreview>\n";
}

bool	PostToReview::Validate(const std::string *scr_params)
{
	if(scr_params[TEXTEDIT].length() > 159)
	{
		EvEdit::MainWindow()->Message("Your message is too long (over 159 characters)!\n");
		return(false);
	}
	
	return(true);
}

void	PostToReview::Update(const std::string *ev_params,const std::string *scr_params)
{
	text = scr_params[TEXTEDIT];
}

void	PostToReview::Write(std::ostringstream& buffer)
{
	buffer << "posttoreview script: text '" << CleanText(text) << "'";
}



