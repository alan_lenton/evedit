/*-----------------------------------------------------------------------
             EvEdit - Windows Federation 2 Script Editor
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "change_link.h"
#include "evedit.h"


const int	ChangeLink::MAX_COMBO_0 = 15;
const int	ChangeLink::MAX_COMBO_1 = 5;

ChangeLink::ChangeLink(const QXmlAttributes *attribs)
{
	if(attribs != 0)
	{
		GetAttribute(attribs,"loc",loc);
		GetAttribute(attribs,"new-loc",new_loc);
		GetAttribute(attribs,"exit",exit);
		GetAttribute(attribs,"action",action);
	}
}

ChangeLink::ChangeLink(const std::string *ev_params,const std::string *scr_params)
{
	loc = scr_params[EDIT0];
	new_loc = scr_params[EDIT1];
	exit = scr_params[COMBO0];
	action = scr_params[COMBO1];
}

ChangeLink::~ChangeLink()
{

}


void	ChangeLink::Display()
{
	std::string	display[Script::MAX_SCRIPT_DISPLAY];

	display[Script::SCRIPT] = "changelink";
	display[Script::LEVEL] = planet_owners;

	display[Script::LABEL0] = "Location #";
	display[Script::EDIT0] = loc;
	display[Script::LABEL1] = "New location";
	display[Script::EDIT1] = new_loc;
	
	// Change MAX_COMBO_0 if you add to this!
	std::string	combo_0[MAX_COMBO_0];
	combo_0[0]  = "Exit direction";
	combo_0[1]  = "0";
	combo_0[2]  = "up";
	combo_0[3]  = "down";
	combo_0[4]  = "in";
	combo_0[5]  = "out";
	combo_0[6]  = "north";
	combo_0[7]  = "north east";
	combo_0[8]  = "east";
	combo_0[9]  = "south east";
	combo_0[10] = "south";
	combo_0[11] = "south west";
	combo_0[12] = "west";
	combo_0[13] = "north west";
	combo_0[14] = "";

	for(auto count = 2; count < 14; ++count)
	{
		if(combo_0[count] == exit)
		{
			std::ostringstream buffer;
			buffer << (count - 2);
			combo_0[1] = buffer.str();
			break;
		}
	}

	// Change MAX_COMBO_1 if you add to this!
	std::string	combo_1[MAX_COMBO_1];
	combo_1[0]  = "Action";
	combo_1[1]  = "0";
	combo_1[2]  = "add";
	combo_1[3]  = "remove";
	combo_1[4]  = "";

	if(action == "remove")
		combo_1[1] = "1";
	

	EvEdit::MainWindow()->DisplayScript(display,combo_0,combo_1);
}

void	ChangeLink::GetComboUpdateInfo(int which_changed,const std::string text,std::string *scr_params)
{
	if(which_changed == COMBO0)
		return;

	if(text == "add")
		scr_params[LABEL1] = "New location";

	if(text == "remove")
		scr_params[LABEL1] = "~disable";
}

void	ChangeLink::Save(std::ofstream& file)
{
	file << "         <changelink loc='" << loc << "'";
	file << " exit='" << exit << "'";
	file << " action='" << action << "'";
	if(action == "add")
		file << " new-loc='" << new_loc << "'";
	file << "/>\n";
}

void	ChangeLink::Update(const std::string *ev_params,const std::string *scr_params)
{
	loc = scr_params[EDIT0];
	new_loc = scr_params[EDIT1];
	exit = scr_params[COMBO0];
	action = scr_params[COMBO1];
}

bool	ChangeLink::Validate(const std::string *scr_params)
{
	if(scr_params[EDIT0] == "")
	{
		EvEdit::MainWindow()->Message("I need to know the location the new exit is from!");
		return(false);
	}

	if((scr_params[EDIT1] == "") && (scr_params[COMBO1] == "add"))
	{
		EvEdit::MainWindow()->Message("I need to know the location to link the new exit to!");
		return(false);
	}

	return true;
}

void	ChangeLink::Write(std::ostringstream& buffer)
{
	buffer << "changelink script:  location " << loc;
	buffer << ":  exit " << exit;
	buffer << ":  action " << action;
	if(action == "add")
		buffer << ":  to location " << new_loc;
}

